// Copyright (c) 2015 Pino Toscano <pino@kde.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version
// 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING.  If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.

#include "mathedit.h"

#include <QApplication>
#include <QKeyEvent>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QStyle>
#include <QStyleOptionFrame>

#include <algorithm>

const QStringList MathHighlighter::allowedVars = QStringList() << "DICE" << "HOUSES" << "GROUPOWNED";

MathHighlighter::MathHighlighter(QTextDocument *parent)
	: QSyntaxHighlighter(parent)
	, m_varsRe("\\$\\{([A-Z]*)\\}")
{
	m_varsRe.optimize();

	m_formatVarBase.setForeground(Qt::darkBlue);

	m_formatVarError = m_formatVarBase;
	m_formatVarError.setFontUnderline(true);
	m_formatVarError.setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);
	m_formatVarError.setUnderlineColor(Qt::red);
}

void MathHighlighter::highlightBlock(const QString &text)
{
	QRegularExpressionMatchIterator it = m_varsRe.globalMatch(text);
	while (it.hasNext())
	{
		const QRegularExpressionMatch match = it.next();
		const int index = match.capturedStart();
		const int length = match.capturedLength();
		const QString var = match.captured(1);
		if (var.isEmpty() || allowedVars.contains(var))
		{
			setFormat(index, length, m_formatVarBase);
		}
		else
		{
			setFormat(index, 2, m_formatVarBase);
			setFormat(index + 2, length - 3, m_formatVarError);
			setFormat(index + length - 1, 1, m_formatVarBase);
		}
	}
}

MathEdit::MathEdit(QWidget *parent)
	: QTextEdit(parent)
{
	setTabChangesFocus(true);
	setWordWrapMode(QTextOption::NoWrap);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	setFixedHeight(sizeHint().height());

	(void) new MathHighlighter(document());

	connect(this, SIGNAL(textChanged()), this, SLOT(slotTextChanged()));
}

QSize MathEdit::sizeHint() const
{
	ensurePolished();
	QFontMetrics fm(font());
	int h = std::max(fm.height(), 14) + document()->documentMargin() * 1;
	int w = fm.horizontalAdvance(QLatin1Char('x')) * 17 + 4;
	QStyleOptionFrame opt;
	opt.initFrom(this);
	opt.lineWidth = lineWidth();
	opt.midLineWidth = midLineWidth();
	return (style()->sizeFromContents(QStyle::CT_LineEdit, &opt, QSize(w, h).
		expandedTo(QApplication::globalStrut()), this));
}

void MathEdit::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
		event->ignore();
	else
		QTextEdit::keyPressEvent(event);
}

void MathEdit::slotTextChanged()
{
	Q_EMIT textChanged(toPlainText());
}

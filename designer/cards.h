// Copyright (c) 2015 Pino Toscano <pino@kde.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version
// 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING.  If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.

#ifndef ATLANTIKDESIGNER_CARDS_H
#define ATLANTIKDESIGNER_CARDS_H

#include <QDialog>

#include "configboard.h"

#include <ui_cards.h>

class QHBoxLayout;
class QSpinBox;
class QCheckBox;
class QComboBox;

class ConfigEstateGroupComboBox;
class MathEdit;

class CardPropertyWidget : public QWidget
{
	Q_OBJECT

	public:
	CardPropertyWidget(ConfigBoard *, int id, Card *, QWidget *parent = nullptr);

	private Q_SLOTS:
	void typeChanged(int);
	void valueChanged(int);
	void estateChanged(int);
	void estateGroupChanged(const QString &);
	void mathChanged(const QString &);

	private:
	Card *card;
	QComboBox *typeCombo;
	QSpinBox *value;
	QComboBox *estate;
	ConfigEstateGroupComboBox *estateGroup;
	MathEdit *math;
	QCheckBox *check;
	CardActionValueType prevType;
	QHBoxLayout *hlayout;

	ConfigBoard *board;

	int id;
};

class CardsEditor : public QDialog
{
	Q_OBJECT

	public:
	CardsEditor(ConfigBoard *board, QWidget *parent = nullptr);

	private Q_SLOTS:
	void slotOk();
	void slotApply();
	void slotChangeStack(int index);
	void slotAddStack();
	void slotChangeCard(int index);
	void slotCardChanged(QListWidgetItem *item);
	void slotAddCard();
	void slotRenameCard();
	void slotDeleteCard();
	void slotAddProperty();
	void slotDeleteProperty();

	private:
	void clearPropertyWidgets();

	Ui::CardsWidget m_ui;

	QVector<CardStack> mylist;

	ConfigBoard *board;

	CardStack *m_currentStack;
	Card *m_currentCard;

	QList<CardPropertyWidget *> m_propertyWidgets;
};

#endif

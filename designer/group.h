#ifndef GROUP_H
#define GROUP_H

#include "configboard.h"

#include <QDialog>
#include <QHash>

class KColorButton;

class QListWidget;
class QListWidgetItem;
class QPushButton;
class QSpinBox;
class QStackedWidget;

class MathEdit;

class EstateGroupConfigWidget : public QWidget
{
	Q_OBJECT

	public:
	EstateGroupConfigWidget(QWidget *parent = nullptr);

	void apply(const ConfigEstateGroup &group);
	void save(ConfigEstateGroup &group) const;

	Q_SIGNALS:
	void changed();

	private:
	MathEdit *rentMathEdit;
	KColorButton *fgButton;
	KColorButton *bgButton;
	QSpinBox *housePrice;
	QSpinBox *globalPrice;
};

class GroupEditor : public QDialog
{
	Q_OBJECT

	public:
	GroupEditor(ConfigBoard *, QWidget *parent = nullptr);

	private Q_SLOTS:
	void slotOk();
	void slotApply();
	void slotChangeItem(QListWidgetItem *item);
	void add();
	void remove();

	private:
	QListWidget *groups;
	QStackedWidget *stack;
	QPushButton *removeB;

	ConfigBoard *board;
	ConfigEstateGroupList mylist;
	QHash<QString, EstateGroupConfigWidget *> configs;
};

#endif

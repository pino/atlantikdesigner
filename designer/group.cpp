#include <qcombobox.h>
#include <qlabel.h>
#include <qspinbox.h>
#include <qstringlist.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QListWidget>
#include <QGroupBox>
#include <QStackedWidget>
#include <QIcon>
#include <QPushButton>
#include <QInputDialog>
#include <QDialogButtonBox>

#include <kcolorbutton.h>
#include <klocalizedstring.h>
#include <kmessagebox.h>
#include <kseparator.h>

#include "group.h"
#include "mathedit.h"

#include <estategroup.h>

EstateGroupConfigWidget::EstateGroupConfigWidget(QWidget *parent)
	: QWidget(parent)
{
	QVBoxLayout *vlayout = new QVBoxLayout(this);
	vlayout->setMargin(0);

	QGroupBox *colorGroupBox = new QGroupBox(i18n("&Colors"), this);
	vlayout->addWidget(colorGroupBox);

	QGridLayout *colorLayout = new QGridLayout(colorGroupBox);
	colorLayout->addWidget(new QLabel(i18n("Foreground:"), colorGroupBox), 0, 0);
	colorLayout->addWidget(fgButton = new KColorButton(colorGroupBox), 0, 1);
	fgButton->setObjectName("Foreground Button");
	connect(fgButton, SIGNAL(changed(QColor)), this, SIGNAL(changed()));

	colorLayout->addWidget(new QLabel(i18n("Background:"), colorGroupBox), 1, 0);
	colorLayout->addWidget(bgButton = new KColorButton(colorGroupBox), 1, 1);
	bgButton->setObjectName("Background Button");
	connect(bgButton, SIGNAL(changed(QColor)), this, SIGNAL(changed()));

	QGroupBox *pricesGroupBox = new QGroupBox(i18n("&Prices"), this);
	vlayout->addWidget(pricesGroupBox);

	QGridLayout *pricesLayout = new QGridLayout(pricesGroupBox);
	pricesLayout->addWidget(new QLabel(i18n("House price:"), pricesGroupBox), 0, 0);
	pricesLayout->addWidget(housePrice = new QSpinBox(pricesGroupBox), 0, 1);
	housePrice->setMinimum(0);
	housePrice->setMaximum(3000);
	housePrice->setSingleStep(25);
	housePrice->setSpecialValueText(i18n("None"));
	housePrice->setSuffix(i18n("$"));
	connect(housePrice, SIGNAL(valueChanged(int)), this, SIGNAL(changed()));

	pricesLayout->addWidget(new QLabel(i18n("Global price:"), pricesGroupBox), 1, 0);
	pricesLayout->addWidget(globalPrice = new QSpinBox(pricesGroupBox), 1, 1);
	globalPrice->setMinimum(0);
	globalPrice->setMaximum(3000);
	globalPrice->setSingleStep(25);
	globalPrice->setSpecialValueText(i18n("None"));
	globalPrice->setSuffix(i18n("$"));
	connect(globalPrice, SIGNAL(valueChanged(int)), this, SIGNAL(changed()));

	QGroupBox *dynamicGroupBox = new QGroupBox(i18n("&Dynamic Rent"), this);
	vlayout->addWidget(dynamicGroupBox);

	QGridLayout *mathLayout = new QGridLayout(dynamicGroupBox);
	mathLayout->addWidget(new QLabel(i18n("Expression:"), dynamicGroupBox), 0, 0);

	rentMathEdit = new MathEdit(dynamicGroupBox);
	connect(rentMathEdit, SIGNAL(textChanged(QString)), this, SIGNAL(changed()));
	mathLayout->addWidget(rentMathEdit, 0, 1);
}

void EstateGroupConfigWidget::apply(const ConfigEstateGroup &group)
{
	fgButton->setColor(group.fgColor());
	bgButton->setColor(group.bgColor());
	housePrice->setValue(group.housePrice());
	globalPrice->setValue(group.globalPrice());
	rentMathEdit->setText(group.rentMath());
}

void EstateGroupConfigWidget::save(ConfigEstateGroup &group) const
{
	group.setFgColor(fgButton->color());
	group.setBgColor(bgButton->color());
	group.setHousePrice(housePrice->value());
	group.setGlobalPrice(globalPrice->value());
	group.setRentMath(rentMathEdit->text());
}

GroupEditor::GroupEditor(ConfigBoard *newBoard, QWidget *parent)
	: QDialog(parent)
	, board(newBoard)
	, mylist(board->estateGroups())
{
	setWindowTitle(i18n("Group Editor"));
	setAttribute(Qt::WA_DeleteOnClose, true);

	QVBoxLayout *mainLayout = new QVBoxLayout(this);

	QHBoxLayout *hlayout = new QHBoxLayout();
	mainLayout->addLayout(hlayout);
	hlayout->setMargin(0);

	QVBoxLayout *listlayout = new QVBoxLayout();
	hlayout->addLayout(listlayout);

	groups = new QListWidget(this);
	listlayout->addWidget(groups);
	connect(groups, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this, SLOT(slotChangeItem(QListWidgetItem*)));

	QHBoxLayout *buttonlayout = new QHBoxLayout();
	listlayout->addLayout(buttonlayout);
	QPushButton *addB = new QPushButton(QIcon::fromTheme("list-add"), QString(), this);
	addB->setToolTip(i18n("Add new group"));
	buttonlayout->addWidget(addB);
	connect(addB, SIGNAL(clicked()), this, SLOT(add()));

	removeB = new QPushButton(QIcon::fromTheme("list-remove"), QString(), this);
	removeB->setToolTip(i18n("Remove group"));
	buttonlayout->addWidget(removeB);
	connect(removeB, SIGNAL(clicked()), this, SLOT(remove()));

	buttonlayout->addStretch();

	mainLayout->addWidget(new KSeparator(this));

	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Apply | QDialogButtonBox::Cancel, this);
	mainLayout->addWidget(buttonBox);

	stack = new QStackedWidget(this);
	stack->setEnabled(false);
	// dummy item, to not let stack be empty
	stack->addWidget(new EstateGroupConfigWidget(stack));
	hlayout->addWidget(stack, 1);

	connect(buttonBox,SIGNAL(rejected()),this,SLOT(reject()));
	connect(buttonBox,SIGNAL(accepted()),this,SLOT(slotOk()));
	connect(buttonBox->button(QDialogButtonBox::Apply),SIGNAL(clicked()),this,SLOT(slotApply()));

	QStringList newgroups;
	foreach (const ConfigEstateGroup &group, mylist)
	{
		const QString name = group.name();
		newgroups.append(name);
		EstateGroupConfigWidget *w = new EstateGroupConfigWidget(stack);
		w->apply(group);
		stack->addWidget(w);
		configs.insert(name, w);
	}
	groups->addItems(newgroups);
}

void GroupEditor::add()
{
	bool ok;
	const QString name = QInputDialog::getText(this, i18n("Add Group"), i18n("Enter the name of the new group:"), QLineEdit::Normal, QString(), &ok);
	if (ok)
	{
		foreach (const ConfigEstateGroup &group, mylist)
		{
			if (group.name() == name)
			{
				KMessageBox::information(this, i18n("That group is already on the list."));
				return;
			}
		}

		mylist.append(ConfigEstateGroup(name));
		groups->addItem(name);
		EstateGroupConfigWidget *w = new EstateGroupConfigWidget(this);
		w->apply(mylist.last());
		stack->addWidget(w);
		configs.insert(name, w);
		stack->setEnabled(true);
	}
}

void GroupEditor::remove()
{
	QListWidgetItem *item = groups->currentItem();
	if (item)
	{
		const QString curText = item->text();
		for (ConfigEstateGroupList::Iterator it =  mylist.begin(); it != mylist.end(); ++it)
		{
			if ((*it).name() == curText)
			{
				mylist.erase(it);
				break;
			}
		}

		delete item;
		delete configs.take(curText);
		stack->setEnabled(!mylist.isEmpty());
	}
}

void GroupEditor::slotChangeItem(QListWidgetItem *item)
{
	if (item)
	{
		stack->setCurrentWidget(configs.value(item->text()));
	}

	stack->setEnabled(item);
	removeB->setEnabled(item);
}

static ConfigEstateGroup &groupOf(ConfigEstateGroupList &groups, const QString &name)
{
	for (ConfigEstateGroupList::Iterator it =  groups.begin(); it != groups.end(); ++it)
		if ((*it).name() == name)
			return *it;
	Q_ASSERT(false);
	static ConfigEstateGroup dummy;
	return dummy;
}

void GroupEditor::slotApply()
{
	QHashIterator<QString, EstateGroupConfigWidget *> it(configs);
	while (it.hasNext())
	{
		it.next();
		it.value()->save(groupOf(mylist, it.key()));
	}

	board->setEstateGroups(mylist);
}

void GroupEditor::slotOk()
{
	slotApply();

	accept();
}

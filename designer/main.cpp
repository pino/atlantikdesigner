#include <QApplication>
#include <QCommandLineParser>
#include <QIcon>

#include <kaboutdata.h>
#include <klocalizedstring.h>
#include "designer.h"

#include <version.h>

int main(int argc, char *argv[])
{
	QApplication kapplication(argc, argv);
	KLocalizedString::setApplicationDomain("atlantikdesigner");

	KAboutData aboutData(
	           "atlantikdesigner",
	           i18n("Atlantik Designer"), ATLANTIKDESIGNER_VERSION_STRING,
	           i18n("Atlantik gameboard designer"),
	           KAboutLicense::GPL,
	           i18n("(c) 2002 Jason Katz-Brown"),
	           QString(),
	           "http://www.unixcode.org/atlantik/"
	           );

	aboutData.addAuthor(i18n("Jason Katz-Brown"), i18n("main author"), "jason@katzbrown.com", "http://katzbrown.com/");
	aboutData.addAuthor(i18n("Rob Kaper"), i18n("libatlantikui"), "cap@capsi.com", "http://capsi.com/");

	KAboutData::setApplicationData(aboutData);

	kapplication.setWindowIcon(QIcon::fromTheme("atlantikdesigner"));

	QCommandLineParser parser;
	parser.addHelpOption();
	parser.addVersionOption();
	aboutData.setupCommandLine(&parser);
	parser.process(kapplication);
	aboutData.processCommandLine(&parser);

	if (kapplication.isSessionRestored())
		kRestoreMainWindows<AtlanticDesigner>();
	else
	{
		AtlanticDesigner *designer = new AtlanticDesigner;
		designer->show();
	}

	return kapplication.exec();
}


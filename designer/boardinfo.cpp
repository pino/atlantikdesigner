#include <qlabel.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QSpinBox>
#include <QIcon>
#include <QPushButton>
#include <QLineEdit>

#include <kcolorbutton.h>
#include <klocalizedstring.h>
#include <kurllabel.h>

#include "boardinfo.h"
#include "configboard.h"

BoardInfoDlg::BoardInfoDlg(bool editable, ConfigBoard *newBoard, QWidget *parent)
	: KPageDialog(parent)
	, board(newBoard)
{
	setWindowTitle(i18n("Gameboard Information"));
	setStandardButtons((editable? QDialogButtonBox::Ok|QDialogButtonBox::Apply|QDialogButtonBox::Cancel : QDialogButtonBox::Close));
	setFaceType(KPageDialog::Tabbed);
	setAttribute(Qt::WA_DeleteOnClose, true);

	const BoardInfo info = board->info();

	QWidget *about = new QWidget();
	addPage(about,i18n("Information"));
	QVBoxLayout *aboutLayout = new QVBoxLayout(about);
	aboutLayout->setMargin(0);

	if (editable)
	{
		aboutLayout->addWidget(name = new QLineEdit(about));
		name->setText(info.name);
	}
	else
	{
		aboutLayout->addWidget(new QLabel(info.name, about));
		name = nullptr;
	}

	QHBoxLayout *versionLayout = new QHBoxLayout();
	aboutLayout->addLayout(versionLayout);
	versionLayout->addWidget(new QLabel(i18n("Version:"), about));
	if (editable)
	{
		versionLayout->addWidget(version = new QLineEdit(about));
		version->setText(info.version);
	}
	else
		versionLayout->addWidget(new QLabel(info.version, about));

	QHBoxLayout *urlLayout = new QHBoxLayout();
	aboutLayout->addLayout(urlLayout);
	urlLayout->addWidget(new QLabel(i18n("URL:"), about));
	if (editable)
		urlLayout->addWidget(url = new QLineEdit(info.url, about));
	else
		urlLayout->addWidget(new KUrlLabel(info.url, info.url, about));

	aboutLayout->addStretch(3);

	aboutLayout->addWidget(new QLabel(i18n("Description:"), about));
	aboutLayout->addStretch();
	aboutLayout->addWidget(description = new QLineEdit(about));
	description->setText(info.description);
	if (!editable)
	{
		description->setReadOnly(true);
	}

	if (editable)
	{
		QHBoxLayout *bgLayout = new QHBoxLayout();
		aboutLayout->addLayout(bgLayout);
		bgLayout->addWidget(new QLabel(i18n("Background color:"), about));
		bgColor = new KColorButton(info.bgColor, about);
		bgLayout->addWidget(bgColor);
	}
	else
		bgColor = nullptr;

	QWidget *game = new QWidget();
	addPage(game, i18n("Game"));
	QGridLayout *gridLayout = new QGridLayout(game);
	gridLayout->setMargin(0);

	gridLayout->addWidget(new QLabel(i18n("Minimum players:"), game), 0, 0);
	gridLayout->addWidget(new QLabel(i18n("Maximum players:"), game), 1, 0);
	gridLayout->addWidget(new QLabel(i18n("Houses:"), game), 2, 0);
	gridLayout->addWidget(new QLabel(i18n("Hotels:"), game), 3, 0);
	gridLayout->addWidget(new QLabel(i18n("Start money:"), game), 4, 0);
	if (editable)
	{
		gridLayout->addWidget(minPlayers = new QSpinBox(game), 0, 1);
		minPlayers->setMinimum(2);
		minPlayers->setMaximum(20);
		minPlayers->setValue(info.minPlayers);
		gridLayout->addWidget(maxPlayers = new QSpinBox(game), 1, 1);
		maxPlayers->setMinimum(2);
		maxPlayers->setMaximum(20);
		maxPlayers->setValue(info.maxPlayers);
		gridLayout->addWidget(houses = new QSpinBox(game), 2, 1);
		houses->setValue(info.houses);
		gridLayout->addWidget(hotels = new QSpinBox(game), 3, 1);
		hotels->setValue(info.hotels);
		gridLayout->addWidget(startMoney = new QSpinBox(game), 4, 1);
		startMoney->setMinimum(0);
		startMoney->setMaximum(100000);
		startMoney->setSpecialValueText(i18n("None"));
		startMoney->setSuffix("$");
		startMoney->setValue(info.startMoney);
	}
	else
	{
		gridLayout->addWidget(new QLabel(QString::number(info.minPlayers), game), 0, 1);
		gridLayout->addWidget(new QLabel(QString::number(info.maxPlayers), game), 1, 1);
		gridLayout->addWidget(new QLabel(QString::number(info.houses), game), 2, 1);
		gridLayout->addWidget(new QLabel(QString::number(info.hotels), game), 3, 1);
		gridLayout->addWidget(new QLabel(QString::number(info.startMoney), game), 4, 1);
	}
	gridLayout->setRowStretch(5, 1);

	QWidget *authorsFrame = new QWidget();
	addPage(authorsFrame,i18n("&Authors"));
	QVBoxLayout *authorsLayout = new QVBoxLayout(authorsFrame);
	authorsLayout->setMargin(0);
	authorsLayout->addWidget(authors = new LotsaEdits(editable, info.authors, authorsFrame));
	authorsLayout->addStretch();

	QWidget *creditsFrame = new QWidget();
	addPage(creditsFrame,i18n("&Thanks To"));
	QVBoxLayout *creditsLayout = new QVBoxLayout(creditsFrame);
	creditsLayout->setMargin(0);
	creditsLayout->addWidget(credits = new LotsaEdits(editable, info.credits, creditsFrame));
	creditsLayout->addStretch();
	connect(buttonBox(),SIGNAL(rejected()),this,SLOT(reject()));
	connect(buttonBox(),SIGNAL(accepted()),this,SLOT(slotOk()));
	if (editable)
		connect(button(QDialogButtonBox::Apply),SIGNAL(clicked()),this,SLOT(slotApply()));
}

void BoardInfoDlg::slotApply()
{
	BoardInfo info;

	info.name = name->text();
	info.description = description->text();
	info.version = version->text();
	info.url = url->text();
	info.authors = authors->readRows();
	info.credits = credits->readRows();
	info.minPlayers = minPlayers->value();
	info.maxPlayers = maxPlayers->value();
	info.houses = houses->value();
	info.hotels = hotels->value();
	info.startMoney = startMoney->value();

	if (bgColor)
		info.bgColor = bgColor->color().name();

	board->setInfo(info);
}

void BoardInfoDlg::slotOk()
{
	slotApply();

	accept();
}

///////////////////////////////////

LotsaEdits::LotsaEdits(bool aeditable, const QStringList &defaults, QWidget *parent)
	: QWidget(parent)
	, editable(aeditable)
{
	QHBoxLayout *mainLayout = new QHBoxLayout(this);
	mainLayout->setMargin(0);

	layout = new QVBoxLayout();
	mainLayout->addLayout(layout);

	QHBoxLayout *hlayout = new QHBoxLayout();
	layout->addLayout(hlayout);

	if (editable)
	{
		QVBoxLayout *buttonLayout = new QVBoxLayout();
		buttonLayout->setAlignment(Qt::AlignTop | Qt::AlignRight);
		mainLayout->addLayout(buttonLayout);
		QPushButton *more = new QPushButton(QIcon::fromTheme("list-add"), QString(), this);
		more->setToolTip(i18n("Add name"));
		QSizePolicy policy = more->sizePolicy();
		policy.setHorizontalPolicy(policy.verticalPolicy());
		more->setSizePolicy(policy);
		buttonLayout->addWidget(more);
		connect(more, SIGNAL(clicked()), this, SLOT(more()));
		lessButton = new QPushButton(QIcon::fromTheme("list-remove"), QString(), this);
		lessButton->setToolTip(i18n("Delete name"));
		lessButton->setEnabled(false);
		policy = lessButton->sizePolicy();
		policy.setHorizontalPolicy(policy.verticalPolicy());
		lessButton->setSizePolicy(policy);
		buttonLayout->addWidget(lessButton);
		connect(lessButton, SIGNAL(clicked()), this, SLOT(less()));
		buttonLayout->addStretch();
	}

	layout->addStretch();

	foreach (const QString &d, defaults)
		newRow(d);
}

QWidget *LotsaEdits::newRow(const QString &text)
{
	QWidget *edit;
	if (editable)
		edit = new QLineEdit(text, this);
	else
		edit = new QLabel(text, this);
	// before the bottom stretch
	layout->insertWidget(layout->count() - 1, edit);
	list.append(edit);
	if (editable)
		lessButton->setEnabled(true);
	return edit;
}

void LotsaEdits::more()
{
	QWidget *row = newRow();
	if (editable)
		row->setFocus();
}

void LotsaEdits::less()
{
	if (list.isEmpty())
		return;

	delete list.takeLast();
	lessButton->setEnabled(!list.isEmpty());
	   /*
	QWidget *edit = nullptr;
	for (edit = list.first(); edit; edit = list.next())
	{
		if (edit->hasFocus())
		{
			list.remove();
			break;
		}
	}
	*/
}

QStringList LotsaEdits::readRows() const
{
	QStringList ret;

	foreach (QWidget *edit, list)
		if (editable)
			ret.append(static_cast<QLineEdit *>(edit)->text());
		else
			ret.append(static_cast<QLabel *>(edit)->text());

	return ret;
}

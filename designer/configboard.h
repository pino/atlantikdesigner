// Copyright (c) 2015 Pino Toscano <pino@kde.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version
// 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING.  If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.

#ifndef ATLANTIKDESIGNER_CONFIGBOARD_H
#define ATLANTIKDESIGNER_CONFIGBOARD_H

#include <QObject>
#include <QVariant>
#include <QVector>

#include <estate.h>

class QIODevice;

enum EstateType { Street = 0, Cards, FreeParking, ToJail, Tax, Jail };
enum CardActionValueType { CAV_Unknown = -1, CAV_Integer = 0, CAV_Money, CAV_Bool, CAV_Estate, CAV_EstateCount, CAV_EstateGroup, CAV_Math };

struct CardAction
{
	const char *key;
	CardActionValueType type;
	const char *description;
};
int findCardAction(const QString &key);
CardActionValueType typeForCardAction(const QString &key);
QStringList allCardActionDescriptions();
const CardAction getCardAction(int i);

class ConfigEstate : public Estate
{
	public:
	ConfigEstate(int estateId);
	void setEstateId(int estateId);
	EstateType type()const { return m_type; }
	void setType(const EstateType type);
	QString group()const { return m_group; }
	void setGroup(const QString &group);
	int rent(int _h)const { return m_rent[_h]; }
	void setRent(int, int);
	int tax()const { return m_tax; }
	void setTax(int);
	int taxPercentage()const { return m_taxPercentage; }
	void setTaxPercentage(int);
	QString takeCard()const { return m_takeCard; }
	void setTakeCard(const QString &);
	QColor forceBg()const { return m_forceBg; }
	void setForceBg(const QColor &);
	bool go()const { return m_go; }
	void setGo(bool);
	int passMoney()const { return m_passMoney; }
	void setPassMoney(int);
	int payAmount()const { return m_payAmount; }
	void setPayAmount(int);

	void setChanged(bool);

	private:
	EstateType m_type : 4;
	bool m_go : 1;
	QString m_group;
	int m_rent[6];
	int m_tax;
	int m_taxPercentage;
	QString m_takeCard;
	QColor m_forceBg;
	int m_passMoney;
	int m_payAmount;
};
typedef QList<ConfigEstate *> EstateList;

class ConfigEstateGroup
{
	public:
	ConfigEstateGroup() { init(); }
	ConfigEstateGroup(const QString &name) { setName(name); init(); }
	void init() { setHousePrice(0); setGlobalPrice(0); }

	void setHousePrice(int newPrice) { m_housePrice = newPrice; }
	int housePrice()const { return m_housePrice; }
	void setGlobalPrice(int newGlobalPrice) { m_globalPrice = newGlobalPrice; }
	int globalPrice()const { return m_globalPrice; }
	QString rentMath()const { return m_rentMath; }
	void setRentMath(const QString &newMath) { m_rentMath = newMath; }
	bool dynamicRent()const { return !m_rentMath.isEmpty(); }
	void setName(const QString &name) { m_name = name; }
	QString name()const { return m_name; }

	QColor fgColor()const { return m_fgColor; }
	void setFgColor(const QColor &color) { m_fgColor = color; }
	QColor bgColor()const { return m_bgColor; }
	void setBgColor(const QColor &color) { m_bgColor = color; }

	private:
	QColor m_fgColor;
	QColor m_bgColor;
	QString m_rentMath;
	int m_housePrice;
	int m_globalPrice;
	QString m_name;
};
typedef QVector<ConfigEstateGroup> ConfigEstateGroupList;

struct CardProperty
{
	CardProperty() {}
	CardProperty(const QString &k, const QVariant &v) : key(k), value(v) {}
	QString key;
	QVariant value;
};

struct Card
{
	Card() {}
	Card(const QString &newName) : name(newName) {}
	QString name;
	QVector<CardProperty> properties;
};

class CardStack : public QVector<Card>
{
	public:
	CardStack() {}
	CardStack(const QString &newName) { setName(newName); }
	QString name()const { return m_name; }
	void setName(const QString &newName) { m_name = newName; }

	private:
	QString m_name;
};

struct BoardInfo
{
	BoardInfo();
	QString name;
	QStringList authors;
	QStringList credits;
	QString description;
	QString url;
	QString version;
	QColor bgColor;
	int minPlayers;
	int maxPlayers;
	int houses;
	int hotels;
	int startMoney;
};

class ConfigBoard : public QObject
{
	Q_OBJECT

	public:
	ConfigBoard();
	~ConfigBoard();

	ConfigEstate *newEstate(int pos);
	ConfigEstate *estateAt(int pos);
	void delEstate(int pos);
	ConfigEstate *findGo() const;
	EstateList estates() const { return m_estates; }

	void setCards(const QVector<CardStack> &cards);
	QVector<CardStack> cards() const { return m_cards; }

	void setInfo(const BoardInfo &info);
	BoardInfo info() const { return m_info; }

	void setEstateGroups(const ConfigEstateGroupList &groups);
	ConfigEstateGroupList estateGroups() const { return m_groups; }

	static ConfigBoard *read(QIODevice *device);
	void save(QIODevice *device) const;

	Q_SIGNALS:
	void cardsChanged();
	void infoChanged();
	void estateGroupsChanged();

	private:
	EstateList m_estates;
	QVector<CardStack> m_cards;
	BoardInfo m_info;
	ConfigEstateGroupList m_groups;
};

#endif

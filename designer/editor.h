#ifndef ATLANTIK_EDITOR_H
#define ATLANTIK_EDITOR_H

#include <qcombobox.h>

class QCheckBox;
class QGridLayout;
class QLineEdit;
class QSpinBox;
class QComboBox;

class KColorButton;

class ConfigBoard;
class ConfigEstate;
class ConfigEstateGroup;

class ConfigEstateGroupComboBox : public QComboBox
{
	Q_OBJECT

	public:
	ConfigEstateGroupComboBox(ConfigBoard *, QWidget *parent = nullptr);

	void setCurrentGroup(const QString &group);

	public Q_SLOTS:
	void refreshGroups();

	private:
	ConfigBoard *board;
};

class CardStackComboBox : public QComboBox
{
	Q_OBJECT

	public:
	CardStackComboBox(ConfigBoard *, QWidget *parent = nullptr);

	void setCurrentCardStack(const QString &stack);

	public Q_SLOTS:
	void refreshCardStacks();

	private:
	ConfigBoard *board;
};

class EstateDlg : public QWidget
{
	Q_OBJECT

	public:
	EstateDlg(QWidget *parent = nullptr) : QWidget(parent) { estate = nullptr; }

	Q_SIGNALS:
	void updateBackground();

	public Q_SLOTS:
	virtual void slotOk() { save(); }
	void slotUpdate(ConfigEstate *newEstate) { estate = newEstate; if (estate) update(); }

	protected:
	virtual void update() {}
	virtual void save() {}
	ConfigEstate *estate;
};

class EstateEdit : public QWidget
{
	Q_OBJECT

	public:
	EstateEdit(ConfigBoard *, QWidget *parent = nullptr);
	ConfigEstate *theEstate() { return estate; }
	bool upArrow()const;
	bool downArrow()const;
	bool leftArrow()const;
	bool rightArrow()const;
	void aboutToDie();
	void setReady(bool);

	public Q_SLOTS:
	void setEstate(ConfigEstate *);
	ConfigEstate *saveEstate(bool superficial = false);

	Q_SIGNALS:
	void somethingChanged();
	void modified();
	void updateBackground();
	void goChecked(int id);
	void resized();

	protected:
	void resizeEvent(QResizeEvent *) override;

	private Q_SLOTS:
	void configure();
	void goToggled(bool);

	private:
	QComboBox *typeCombo;
	QLineEdit *nameEdit;
	QCheckBox *goCheck;
	QSpinBox *passMoney;
	QLineEdit *iconEdit;
	QWidget *centerWidget;
	QGridLayout *layout;

	ConfigBoard *board;

	ConfigEstate *estate;

	EstateDlg *confDlg;

	int oldType;

	bool ready;
	bool locked;
};

class TaxDlg : public EstateDlg
{
	Q_OBJECT

	public:
	TaxDlg(QWidget *parent = nullptr);

	protected:
	void save() override;
	void update() override;

	private:
	QSpinBox *tax;
	QSpinBox *taxPercentage;
};

class GenericDlg : public EstateDlg
{
	Q_OBJECT

	public:
	GenericDlg(QWidget *parent = nullptr);

	protected:
	void save() override;
	void update() override;

	private:
	KColorButton *col;
};

class CardsDlg : public EstateDlg
{
	Q_OBJECT

	public:
	CardsDlg(ConfigBoard *, QWidget *parent = nullptr);

	protected:
	void save() override;
	void update() override;

	private:
	ConfigBoard *board;

	CardStackComboBox *cards;
};

class StreetDlg : public EstateDlg
{
	Q_OBJECT

	public:
	StreetDlg(ConfigBoard *, QWidget *parent = nullptr);

	protected:
	void save() override;
	void update() override;

	private Q_SLOTS:
	void groupChanged(const QString &);

	private:
	QSpinBox *houses0;
	QSpinBox *houses1;
	QSpinBox *houses2;
	QSpinBox *houses3;
	QSpinBox *houses4;
	QSpinBox *houses5;
	QSpinBox *price;
	ConfigEstateGroupComboBox *groupCombo;

	ConfigBoard *board;
};

class JailDlg : public GenericDlg
{
	Q_OBJECT

	public:
	JailDlg(QWidget *parent = nullptr);

	protected:
	void save() override;
	void update() override;

	private:
	QSpinBox *payAmount;
};

#endif

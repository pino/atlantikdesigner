// Copyright (c) 2015 Pino Toscano <pino@kde.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version
// 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING.  If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.

#include "configboard.h"

#include <QSet>
#include <QTextCodec>
#include <QTextStream>

#include <klocalizedstring.h>

#include <designer_debug.h>

static const CardAction cardtypes[] = {
	{ "pay", CAV_Money, I18N_NOOP("Pay") },
	{ "payeach", CAV_Money, I18N_NOOP("Pay Each Player") },
	{ "collect", CAV_Money, I18N_NOOP("Collect") },
	{ "collecteach", CAV_Money, I18N_NOOP("Collect From Each Player") },
	{ "advanceto", CAV_Estate, I18N_NOOP("Advance To") },
	{ "advance", CAV_EstateCount, I18N_NOOP("Advance") },
	{ "goback", CAV_EstateCount, I18N_NOOP("Go Back") },
	{ "tojail", CAV_Bool, I18N_NOOP("Go to Jail") },
	{ "outofjail", CAV_Bool, I18N_NOOP("Get out of Jail Free Card") },
	{ "nextutil", CAV_Bool, I18N_NOOP("Advance to Nearest Utility") },
	{ "nextrr", CAV_Bool, I18N_NOOP("Advance to Nearest Railroad") },
	{ "payhouse", CAV_Money, I18N_NOOP("Pay for Each House") },
	{ "payhotel", CAV_Money, I18N_NOOP("Pay for Each Hotel") },
	{ "canbeowned", CAV_Bool, I18N_NOOP("Can Be Owned") },
	{ "advancetonextof", CAV_EstateGroup, I18N_NOOP("Advance to Next Of") },
	{ "rentmath", CAV_Math, I18N_NOOP("Dynamic Rent") },
	{ nullptr, CAV_Unknown, nullptr },
};

int findCardAction(const QString &key)
{
	for (const CardAction *ca = cardtypes; ca->key; ++ca)
		if (ca->key == key)
			return ca - cardtypes;
	return -1;
}

CardActionValueType typeForCardAction(const QString &key)
{
	for (const CardAction *ca = cardtypes; ca->key; ++ca)
		if (ca->key == key)
			return ca->type;
	return CAV_Unknown;
}

QStringList allCardActionDescriptions()
{
	QStringList items;
	for (const CardAction *ca = cardtypes; ca->key; ++ca)
		items.append(i18n(ca->description));
	return items;
}

const CardAction getCardAction(int i)
{
	return cardtypes[i];
}

///////////////////////////

ConfigEstate::ConfigEstate(int estateId)
	: Estate(estateId)
	, m_type(Street)
	, m_go(false)
	, m_group("Default")
	, m_tax(0)
	, m_taxPercentage(0)
	, m_passMoney(0)
	, m_payAmount(0)
{
	m_rent[0] = m_rent[1] = m_rent[2] = m_rent[3] = m_rent[4] = m_rent[5] = 0;
	m_changed = false;
}

void ConfigEstate::setChanged(bool b)
{
	m_changed = b;
}

void ConfigEstate::setEstateId(int estateId)
{
	if (m_id != estateId)
	{
		m_id = estateId;
		m_changed = true;
	}
}

void ConfigEstate::setType(const EstateType type)
{
	if (m_type != type)
	{
		m_type = type;
		m_changed = true;
	}
}

void ConfigEstate::setGroup(const QString &group)
{
	if (m_group != group)
	{
		m_group = group;
		m_changed = true;
	}
}

void ConfigEstate::setRent(int houses, int rent)
{
	if (m_rent[houses] != rent)
	{
		m_rent[houses] = rent;
		m_changed = true;
	}
}

void ConfigEstate::setTax(int tax)
{
	if (m_tax != tax)
	{
		m_tax = tax;
		m_changed = true;
	}
}

void ConfigEstate::setTaxPercentage(int taxPercentage)
{
	if (m_taxPercentage != taxPercentage)
	{
		m_taxPercentage = taxPercentage;
		m_changed = true;
	}
}

void ConfigEstate::setTakeCard(const QString &takeCard)
{
	if (m_takeCard != takeCard)
	{
		m_takeCard = takeCard;
		m_changed = true;
	}
}

void ConfigEstate::setForceBg(const QColor &forceBg)
{
	if (m_forceBg != forceBg)
	{
		m_forceBg = forceBg;
		m_changed = true;
	}
}

void ConfigEstate::setGo(bool go)
{
	if (m_go != go)
	{
		m_go = go;
		m_changed = true;
	}
}

void ConfigEstate::setPassMoney(int passMoney)
{
	if (m_passMoney != passMoney)
	{
		m_passMoney = passMoney;
		m_changed = true;
	}
}

void ConfigEstate::setPayAmount(int payAmount)
{
	if (m_payAmount != payAmount)
	{
		m_payAmount = payAmount;
		m_changed = true;
	}
}

///////////////////////////

BoardInfo::BoardInfo()
	: bgColor(Qt::lightGray)
	, minPlayers(2)
	, maxPlayers(6)
	, houses(32)
	, hotels(16)
	, startMoney(1500)
{
}

///////////////////////////

ConfigBoard::ConfigBoard()
	: QObject()
{
}

ConfigBoard::~ConfigBoard()
{
	qDeleteAll(m_estates);
}

ConfigEstate *ConfigBoard::newEstate(int pos)
{
	ConfigEstate *estate = new ConfigEstate(pos);
	m_estates.insert(pos, estate);
	return estate;
}

ConfigEstate *ConfigBoard::estateAt(int pos)
{
	return m_estates.value(pos, nullptr);
}

void ConfigBoard::delEstate(int pos)
{
	delete m_estates.takeAt(pos);
}

ConfigEstate *ConfigBoard::findGo() const
{
	foreach (ConfigEstate *e, m_estates)
		if (e->go())
			return e;

	return nullptr;
}
void ConfigBoard::setCards(const QVector<CardStack> &cards)
{
	m_cards = cards;
	Q_EMIT cardsChanged();
}

void ConfigBoard::setInfo(const BoardInfo &info)
{
	m_info = info;
	Q_EMIT infoChanged();
}

void ConfigBoard::setEstateGroups(const ConfigEstateGroupList &groups)
{
	m_groups = groups;
	Q_EMIT estateGroupsChanged();
}

ConfigBoard *ConfigBoard::read(QIODevice *device)
{
	enum ParseMode { Parse_Nothing, Parse_General, Parse_Board, Parse_Cards, Parse_EstateGroups, Parse_Estates };

	QTextStream t(device);
	t.setCodec(QTextCodec::codecForName("utf8"));

	QString s = t.readLine();
	ParseMode parseMode = Parse_Nothing;

	ConfigBoard *board = new ConfigBoard();
	CardStack *curCardStack = nullptr;

	int goEstate = 0;
	int i;
	for (i = 0; !t.atEnd();)
	{
		s = s.trimmed();

		if (s.isEmpty())
		{
			s = t.readLine();
			continue;
		}

		QString name;

		if (s.left(1) == "<")
		{
			if (s == "<General>")
				parseMode = Parse_General;
			else if (s == "<Board>")
				parseMode = Parse_Board;
			else if (s == "<EstateGroups>")
				parseMode = Parse_EstateGroups;
			else if (s == "<Cards>")
				parseMode = Parse_Cards;
			else if (s == "<Estates>")
				parseMode = Parse_Estates;
			else
				qCDebug(DESIGNER_LOG) << "unknown config group:" << s;

			s = t.readLine();
			continue;
		}

		// this for outside-of-[]-settings
		int eqSign = s.indexOf("=");
		if (eqSign >= 0)
		{
			QString key = s.left(eqSign);
			QString value = s.right(s.length() - eqSign - 1);

			if (parseMode == Parse_Cards)
			{
				if (key == "groupname")
				{
					board->m_cards.append(CardStack(value));
					curCardStack = &board->m_cards.last();
				}
				else
					qCDebug(DESIGNER_LOG) << "unknown <Cards> property:" << key;
			}
			else if (parseMode == Parse_General)
			{
				if (key == "name")
					board->m_info.name = value;
				else if (key == "description")
					board->m_info.description = value;
				else if (key == "version")
					board->m_info.version = value;
				else if (key == "url")
					board->m_info.url = value;
				else if (key == "authors")
#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
					board->m_info.authors = value.split(",", Qt::SkipEmptyParts);
#else
					board->m_info.authors = value.split(",", QString::SkipEmptyParts);
#endif
				else if (key == "credits")
#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
					board->m_info.credits = value.split(",", Qt::SkipEmptyParts);
#else
					board->m_info.credits = value.split(",", QString::SkipEmptyParts);
#endif
				else if (key == "minplayers")
					board->m_info.minPlayers = value.toInt();
				else if (key == "maxplayers")
					board->m_info.maxPlayers = value.toInt();
				else if (key == "houses")
					board->m_info.houses = value.toInt();
				else if (key == "hotels")
					board->m_info.hotels = value.toInt();
				else if (key == "startmoney")
					board->m_info.startMoney = value.toInt();
				else
					qCDebug(DESIGNER_LOG) << "unknown <General> property:" << key;
			}
			else if (parseMode == Parse_Board)
			{
				if (key == "go")
					goEstate = value.toInt();
				else if (key == "bgcolor")
					board->m_info.bgColor.setNamedColor(value);
				else
					qCDebug(DESIGNER_LOG) << "unknown <Board> property:" << key;
			}

			s = t.readLine();
			continue;
		}

		name = s.left(s.indexOf("]"));
		name = name.right(name.length() - name.indexOf("[") - 1);

		if (name.isEmpty())
			name = i18n("No Name");

		// for estates
		EstateType type = FreeParking;
		QString group, takeCard;
		int price = 0;
		int rent[6] = {0, 0, 0, 0, 0, 0};
		int tax = 0;
		int taxPercentage = 0;
		int passMoney = 0;
		int payAmount = 0;
		QColor forceBg;
		QString icon;

		// for groups
		QColor color, bgColor;
		QString rentMath;
		int housePrice = 0;
		int globalPrice = 0;

		// for cards
		QVector<CardProperty> properties;

		while (true)
		{
			if (t.atEnd())
				break;

			s = t.readLine().trimmed();

			if (s.left(1) == "[" || s.left(1) == "<")
				break;

			int eqSign = s.indexOf("=");
			if (eqSign < 0)
				continue;

			QString key = s.left(eqSign);
			QString value = s.right(s.length() - eqSign - 1);

			//////////////////////////////// ESTATES
			if (parseMode == Parse_Estates)
			{
				if (key == "price")
					price = value.toInt();
				else if (key.left(4) == "rent")
				{
					int houses = key.right(1).toInt();
					if (houses < 0 || houses > 5)
						continue;

					rent[houses] = value.toInt();
				}
				else if (key == "tax")
				{
					tax = value.toInt();
					type = Tax;
				}
				else if (key == "taxpercentage")
				{
					taxPercentage = value.toInt();
					type = Tax;
				}
				else if (key == "group")
				{
					group = value;
					type = Street;
				}
				else if (key == "tojail")
					type = ToJail;
				else if (key == "jail")
					type = Jail;
				else if (key == "freeparking")
					type = FreeParking;
				else if (key == "takecard")
				{
					takeCard = value;
					type = Cards;
				}
				else if (key == "bgcolor")
					forceBg = QColor(value);
				else if (key == "passmoney")
					passMoney = value.toInt();
				else if (key == "icon")
					icon = value;
				else if (key == "payamount")
					payAmount = value.toInt();
				else
					qCDebug(DESIGNER_LOG) << "unknown estate property:" << key;
			}
			else if (parseMode == Parse_EstateGroups)
			{
				if (key == "color")
					color.setNamedColor(value);
				else if (key == "bgcolor")
					bgColor.setNamedColor(value);
				else if (key == "houseprice")
					housePrice = value.toInt();
				else if (key == "price")
					globalPrice = value.toInt();
				else if (key == "rentmath")
					rentMath = value;
				else
					qCDebug(DESIGNER_LOG) << "unknown estate group property:" << key;
			}
			else if (parseMode == Parse_Cards)
			{
				CardActionValueType cardtype = typeForCardAction(key);
				QVariant v;
				switch (cardtype)
				{
				case CAV_Unknown:
					qCDebug(DESIGNER_LOG) << "unknown card action:" << key;
					break;

				case CAV_Integer:
				case CAV_Money:
				case CAV_Bool:
				case CAV_Estate:
				case CAV_EstateCount:
				{
					bool ok;
					int val = value.toInt(&ok);
					if (!ok)
						continue;

					if (key == "pay" && val < 0)
					{
						val *= -1;
						key = "collect";
					}
					else if (key == "payeach" && val < 0)
					{
						val *= -1;
						key = "collecteach";
					}
					else if (key == "advance" && val < 0)
					{
						val *= -1;
						key = "goback";
					}
					v = val;
					break;
				}

				case CAV_EstateGroup:
				case CAV_Math:
					v = value;
					break;
				}

				if (!v.isNull())
					properties.append(CardProperty(key, v));
			}
		}

		if (parseMode == Parse_Estates)
		{
			ConfigEstate *estate = board->newEstate(i);
			estate->setName(name);
			estate->setType(type);
			if (!group.isEmpty())
				estate->setGroup(group);
			estate->setTakeCard(takeCard);
			estate->setPrice(price);
			for (int j = 0; j < 6; j++)
				estate->setRent(j, rent[j]);
			estate->setTax(tax);
			estate->setTaxPercentage(taxPercentage);
			estate->setForceBg(forceBg);
			estate->setPassMoney(passMoney);
			estate->setIcon(icon);
			estate->setPayAmount(payAmount);

			i++;
		}
		else if (parseMode == Parse_Cards)
		{
			Card card;
			card.name = name;
			card.properties = properties;
			if (curCardStack)
				curCardStack->append(card);
		}
		else if (parseMode == Parse_EstateGroups)
		{
			bool found = false;
			foreach (const ConfigEstateGroup &ceg, board->m_groups)
			{
				if (ceg.name() == name)
				{
					found = true;
					break;
				}
			}

			if (found)
				continue;

			ConfigEstateGroup group(name);
			group.setHousePrice(housePrice);
			group.setGlobalPrice(globalPrice);
			group.setFgColor(color);
			group.setBgColor(bgColor);
			group.setRentMath(rentMath);
			board->m_groups.append(group);
		}
	}

	ConfigEstate *newGo = board->estateAt(goEstate);
	if (newGo)
		newGo->setGo(true);

	return board;
}

void ConfigBoard::save(QIODevice *device) const
{
	QTextStream t(device);
	t.setCodec(QTextCodec::codecForName("utf8"));

	t << "<General>\n\n";
	if (!m_info.name.isEmpty())
		t << "name=" << m_info.name << '\n';
	if (!m_info.description.isEmpty())
		t << "description=" << m_info.description << '\n';
	if (!m_info.url.isEmpty())
		t << "url=" << m_info.url << '\n';
	if (!m_info.version.isEmpty())
		t << "version=" << m_info.version << '\n';
	if (!m_info.authors.isEmpty())
		t << "authors=" << m_info.authors.join(",") << '\n';
	if (!m_info.credits.isEmpty())
		t << "credits=" << m_info.credits.join(",") << '\n';
	t << "minplayers=" << m_info.minPlayers << '\n';
	t << "maxplayers=" << m_info.maxPlayers << '\n';
	t << "houses=" << m_info.houses << '\n';
	t << "hotels=" << m_info.hotels << '\n';
	t << "startmoney=" << m_info.startMoney << '\n';

	t << '\n';
	t << "<Board>\n\n";

	ConfigEstate *go = findGo();
	t << "go=" << (go ? go->id() : 0) << '\n';

	if (m_info.bgColor.isValid())
		t << "bgcolor=" << m_info.bgColor.name() << '\n';

	t << "\n\n";

	t << "<EstateGroups>\n";

	// double-prevent duplicates
	QSet<QString> writtenGroups;

	foreach (const ConfigEstateGroup &group, m_groups)
	{
		if (writtenGroups.contains(group.name()) > 0)
			continue;

		if (group.name() == "Default")
		{
			bool containsDefault = false;
			foreach (ConfigEstate *estate, m_estates)
			{
				if (estate->group() == "Default")
				{
					containsDefault = true;
					break;
				}
			}

			if (!containsDefault)
				continue;
		}

		writtenGroups.insert(group.name());

		t << QString("\n[%1]\n").arg(group.name());

		if (group.globalPrice() > 0)
			t << "price=" << group.globalPrice() << '\n';

		if (group.fgColor().isValid())
			t << "color=" << group.fgColor().name() << '\n';
		if (group.bgColor().isValid())
			t << "bgcolor=" << group.bgColor().name() << '\n';

		if (group.housePrice() > 0)
			t << "houseprice=" << group.housePrice() << '\n';

		if (!group.rentMath().isEmpty())
			t << "rentmath=" << group.rentMath() << '\n';
	}

	// now do the cards

	foreach (const CardStack &stack, m_cards)
	{
		t << "\n\n";
		t << "<Cards>\n\n";
		t << "groupname=" << stack.name() << '\n';

		foreach (const Card &card, stack)
		{
			t << "\n[" << card.name << "]\n";
			foreach (const CardProperty &property, card.properties)
			{
				QString key = property.key;
				CardActionValueType cardtype = typeForCardAction(key);
				QVariant val = property.value;

				switch (cardtype)
				{
				case CAV_Unknown:
					Q_ASSERT_X(false, "ConfigBoard::save", "CAV_Unknown");
					break;

				case CAV_Integer:
				case CAV_Money:
				case CAV_Estate:
				case CAV_EstateCount:
				{
					int value = val.toInt();
					if (key == "collect")
					{
						value *= -1;
						key = "pay";
					}
					else if (key == "collecteach")
					{
						value *= -1;
						key = "payeach";
					}
					else if (key == "goback")
					{
						value *= -1;
						key = "advance";
					}
					t << key << "=" << value << '\n';
					break;
				}

				case CAV_Bool:
					t << key << "=" << 1 << '\n';
					break;

				case CAV_EstateGroup:
				case CAV_Math:
					t << key << "=" << val.toString() << '\n';
					break;
				}
			}
		}
	}

	t << "\n\n";

	t << "<Estates>\n";

	foreach (ConfigEstate *estate, m_estates)
	{
		t << QString("\n[%1]\n").arg(estate->name());

		switch (estate->type())
		{
			case Street:
				if (!estate->group().isNull())
					t << "group=" << estate->group() << '\n';

				if (estate->price() > 0)
					t << "price=" << estate->price() << '\n';

				for (int i = 0; i < 6; i++)
				{
					if (estate->rent(i) > 0)
						t << "rent" << i << "=" << estate->rent(i) << '\n';
				}
				break;

			case Tax:
				if (estate->tax() > 0)
					t << "tax=" << estate->tax() << '\n';
				if (estate->taxPercentage() > 0)
					t << "taxpercentage=" << estate->taxPercentage() << '\n';
				break;

			case Jail:
				t << "jail=1\n";
				if (estate->forceBg().isValid())
					t << "bgcolor=" << estate->forceBg().name() << '\n';
				if (estate->payAmount() > 0)
					t << "payamount=" << estate->payAmount() << '\n';
				break;

			case ToJail:
				t << "tojail=1\n";
				if (estate->forceBg().isValid())
					t << "bgcolor=" << estate->forceBg().name() << '\n';
				break;

			case Cards:
				t << "takecard=" << estate->takeCard() << '\n';
				break;

			case FreeParking:
				if (estate->forceBg().isValid())
					t << "bgcolor=" << estate->forceBg().name() << '\n';
				break;
		}

		if (estate->passMoney() > 0)
			t << "passmoney=" << estate->passMoney() << '\n';

		if (!estate->icon().isEmpty())
			t << "icon=" << estate->icon() << '\n';
	}
}

#ifndef ATLANTK_DESIGNER_H
#define ATLANTK_DESIGNER_H

#include <qcolor.h>
#include <qpointer.h>
#include <qstring.h>

#include <kxmlguiwindow.h>

#include <tokentheme.h>

class EstateEdit;
class KSelectAction;
class KRecentFilesAction;
class Estate;
class Player;
class ConfigEstate;
class AtlantikBoard;
class ConfigBoard;
class GroupEditor;
class BoardInfoDlg;
class CardsEditor;

class AtlanticDesigner : public KXmlGuiWindow
{
	Q_OBJECT

	public:
	AtlanticDesigner(QWidget *parent = nullptr);
	~AtlanticDesigner();

	protected:
	void closeEvent(QCloseEvent *) override;

	public Q_SLOTS:
	void modified();

	private Q_SLOTS:
	void open();
	void openRecent(const QUrl &);
	void openNew();
	void save();
	void saveAs();
	void copy();
	void paste();
	void changeEstate(int);
	void changeEstate(Estate *);
	void movePlayer(Estate *);
	void setPlayerAtBeginning();
	void info();
	void editGroups();
	void groupsChanged();
	void goChanged(int);
	void updateBackground();
	void editCards();

	void up();
	void down();
	void left();
	void right();

	void smaller();
	void larger();

	void updateJumpMenu();

	private:
	bool openFile(const QString &);
	bool warnClose();
	void initBoard();
	void setConfigBoard(ConfigBoard *newBoard = nullptr);
	void initToken();
	ConfigEstate *newEstate(int);

	QPointer<EstateEdit> editor;
	QPointer<AtlantikBoard> board;
	QPointer<GroupEditor> groupEditor;
	QPointer<BoardInfoDlg> boardInfoDlg;
	QPointer<CardsEditor> cardsEditor;
	KSelectAction *estateAct;
	KRecentFilesAction *recentAct;

	ConfigBoard *boardData;

	QString m_filename;

	ConfigEstate *copiedEstate;

	void doCaption(bool);

	int max;

	Player *m_player;

	bool isMod;

	QColor defaultFg;
	QColor defaultBg;

	TokenTheme m_tokenTheme;
};

#endif

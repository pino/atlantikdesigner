#include <qcheckbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qspinbox.h>
#include <qstringlist.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QListWidget>
#include <QGroupBox>
#include <QComboBox>

#include <kcolorbutton.h>
#include <klocalizedstring.h>
#include <kseparator.h>

#include "editor.h"
#include "configboard.h"

ConfigEstateGroupComboBox::ConfigEstateGroupComboBox(ConfigBoard *newBoard, QWidget *parent)
	: QComboBox(parent)
	, board(newBoard)
{
	refreshGroups();
	connect(board, SIGNAL(estateGroupsChanged()), this, SLOT(refreshGroups()));
}

void ConfigEstateGroupComboBox::setCurrentGroup(const QString &group)
{
	int newIndex = -1;

	if (!group.isEmpty())
		newIndex = findData(group);

	setCurrentIndex(newIndex);
}

void ConfigEstateGroupComboBox::refreshGroups()
{
	const QString oldGroup = currentText();

	int i = 0;
	int newIndex = -1;
	clear();
	foreach (const ConfigEstateGroup &ceg, board->estateGroups())
	{
		const QString group = ceg.name();
		// use the user data as reliable way to store the group
		addItem(group, group);
		if (group == oldGroup)
			newIndex = i;
		++i;
	}

	setCurrentIndex(newIndex);
}

///////////////////////////

CardStackComboBox::CardStackComboBox(ConfigBoard *newBoard, QWidget *parent)
	: QComboBox(parent)
	, board(newBoard)
{
	refreshCardStacks();
	connect(board, SIGNAL(cardsChanged()), this, SLOT(refreshCardStacks()));
}

void CardStackComboBox::setCurrentCardStack(const QString &stack)
{
	int newIndex = -1;

	if (!stack.isEmpty())
		newIndex = findData(stack);

	setCurrentIndex(newIndex);
}

void CardStackComboBox::refreshCardStacks()
{
	const QString oldStack = currentText();

	int i = 0;
	int newIndex = -1;
	clear();
	foreach (const CardStack &stack, board->cards())
	{
		const QString name = stack.name();
		// use the user data as reliable way to store the name
		addItem(name, name);
		if (name == oldStack)
			newIndex = i;
		++i;
	}

	setCurrentIndex(newIndex);
}

///////////////////////////

EstateEdit::EstateEdit(ConfigBoard *newBoard, QWidget *parent)
	: QWidget(parent)
	, board(newBoard)
	, estate(nullptr)
	, confDlg(nullptr)
	, oldType(-1)
	, ready(false)
	, locked(false)
{
	connect(this, SIGNAL(somethingChanged()), this, SLOT(saveEstate()));

	layout = new QGridLayout(this);
	layout->setMargin(0);
	layout->setColumnStretch(0, 1);
	layout->setColumnStretch(1, 1);
	nameEdit = new QLineEdit(this);
	nameEdit->setObjectName("Name Edit");
	layout->addWidget(nameEdit, 0, 0, 1, 2);
	connect(nameEdit, SIGNAL(returnPressed()), this, SIGNAL(somethingChanged()));

	layout->setRowStretch(2, 2);

	layout->addWidget(new KSeparator(this), 3, 0, 1, 2);

	QLabel *typeLabel = new QLabel(i18n("Type:"), this);
	layout->addWidget(typeLabel, 4, 0);
	typeCombo = new QComboBox( this );
	typeCombo->setObjectName( "Type Combo" );
	layout->addWidget(typeCombo, 4, 1);
	connect(typeCombo, SIGNAL(activated(int)), this, SIGNAL(somethingChanged()));
	connect(typeCombo, SIGNAL(activated(int)), this, SIGNAL(updateBackground()));

	goCheck = new QCheckBox(i18n("This estate is 'Go'"), this);
	connect(goCheck, SIGNAL(toggled(bool)), this, SIGNAL(somethingChanged()));
	connect(goCheck, SIGNAL(toggled(bool)), this, SLOT(goToggled(bool)));
	layout->addWidget(goCheck, 5, 0, 1, 2);

	QLabel *passMoneyLabel = new QLabel(i18n("Pass money:"), this);
	layout->addWidget(passMoneyLabel, 6, 0);
	passMoney = new QSpinBox(this);
	passMoney->setMinimum(0);
	passMoney->setMaximum(3000);
	passMoney->setSingleStep(10);
	passMoney->setSpecialValueText(i18n("None"));
	passMoney->setSuffix("$");
	layout->addWidget(passMoney, 6, 1);

	layout->addWidget(new QLabel(i18n("Icon:"), this), 7, 0);
	iconEdit = new QLineEdit(this);
	connect(iconEdit, SIGNAL(returnPressed()), this, SIGNAL(somethingChanged()));
	layout->addWidget(iconEdit, 7, 1);

	QStringList estateTypes(i18n("Street"));
	estateTypes.append(i18n("Cards"));
	estateTypes.append(i18n("Free Parking"));
	estateTypes.append(i18n("Go to Jail"));
	estateTypes.append(i18n("Tax"));
	estateTypes.append(i18n("Jail"));
	typeCombo->addItems(estateTypes);
}

void EstateEdit::aboutToDie()
{
	delete confDlg;
	confDlg = nullptr;
}

void EstateEdit::setReady(bool ready)
{
	this->ready = ready;
}

void EstateEdit::resizeEvent(QResizeEvent *)
{
	Q_EMIT resized();
}

void EstateEdit::setEstate(ConfigEstate *_estate)
{
	if (!_estate)
		return;

	// why the hell is this here? :-)
	while (1)
		if (!locked)
			break;

	estate = _estate;

	ready = false;
	nameEdit->setText(estate->name());
	typeCombo->setCurrentIndex(estate->type());
	goCheck->setChecked(estate->go());
	passMoney->setValue(estate->passMoney());
	iconEdit->setText(estate->icon());

	ready = true;

	saveEstate(true);
}

ConfigEstate *EstateEdit::saveEstate(bool superficial)
{
	if (!estate || !ready)
		return nullptr;

	locked = true;

	EstateType curType = (EstateType)typeCombo->currentIndex();

	if (!superficial)
	{
		confDlg->slotOk();

		estate->setType(curType);
		estate->setName(nameEdit->text());
		estate->setGo(goCheck->isChecked());
		estate->setPassMoney(passMoney->value());
		estate->setIcon(iconEdit->text());
	}

	if (curType != Street)
	{
	}

	if (!superficial)
		estate->update();

	configure();

	locked = false;

	return estate;
}

void EstateEdit::configure()
{
	if (oldType == typeCombo->currentIndex())
	{
		confDlg->slotUpdate(estate);
		return;
	}

	delete confDlg;

	switch (estate->type())
	{
	case Street:
		confDlg = new StreetDlg(board, this);
		break;

	case Tax:
		confDlg = new TaxDlg(this);
		break;

	case Cards:
		confDlg = new CardsDlg(board, this);
		break;

	case Jail:
		confDlg = new JailDlg(this);
		break;

	case ToJail:
	case FreeParking:
		confDlg = new GenericDlg(this);
		break;

	default:
		confDlg = new EstateDlg(this);
		break;
	}

	confDlg->slotUpdate(estate);

	connect(confDlg, SIGNAL(updateBackground()), this, SIGNAL(updateBackground()));

	layout->addWidget(confDlg, 1, 0, 1, 2);
	confDlg->show();

	oldType = typeCombo->currentIndex();
}

void EstateEdit::goToggled(bool on)
{
	if (on)
	{
		Q_EMIT goChecked(estate->id());
	}
}

bool EstateEdit::upArrow()const
{
	return (nameEdit->hasFocus() || typeCombo->hasFocus());
}

bool EstateEdit::downArrow()const
{
	return upArrow();
}

bool EstateEdit::leftArrow()const
{
	if (nameEdit->hasFocus())
	{
		nameEdit->setCursorPosition(nameEdit->cursorPosition() - 1);
		return true;
	}
	return false;
}

bool EstateEdit::rightArrow()const
{
	if (nameEdit->hasFocus())
	{
		nameEdit->setCursorPosition(nameEdit->cursorPosition() + 1);
		return true;
	}
	return false;
}

/////////////////////////////////

TaxDlg::TaxDlg(QWidget *parent)
	: EstateDlg(parent)
{
	QGridLayout *taxBox = new QGridLayout(this);
	taxBox->setMargin(0);
	taxBox->addWidget(new QLabel(i18n("Fixed tax:"), this), 0, 0);
	taxBox->addWidget(tax = new QSpinBox(this), 0, 1);
	tax->setMinimum(0);
	tax->setMaximum(3000);
	tax->setSpecialValueText(i18n("None"));
	tax->setSuffix("$");
	taxBox->addWidget(new QLabel(i18n("Percentage tax:"), this), 1, 0);
	taxBox->addWidget(taxPercentage = new QSpinBox(this), 1, 1);
	taxPercentage->setMinimum(0);
	taxPercentage->setMaximum(100);
	taxPercentage->setSpecialValueText(i18n("None"));
	taxPercentage->setSuffix("%");
}

void TaxDlg::save()
{
	estate->setTax(tax->value());
	estate->setTaxPercentage(taxPercentage->value());
}

void TaxDlg::update()
{
	tax->setValue(estate->tax());
	taxPercentage->setValue(estate->taxPercentage());
}

/////////////////////////////////

GenericDlg::GenericDlg(QWidget *parent)
	: EstateDlg(parent)
{
	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->setMargin(0);

	QHBoxLayout *hlayout = new QHBoxLayout();
	layout->addLayout(hlayout);
	hlayout->addWidget(new QLabel(i18n("Background:"), this));

	col = new KColorButton(this);
	hlayout->addWidget(col);
}

void GenericDlg::save()
{
	estate->setForceBg(col->color());
	Q_EMIT updateBackground();
}

void GenericDlg::update()
{
	col->setColor(estate->forceBg());
}

/////////////////////////////////

CardsDlg::CardsDlg(ConfigBoard *newBoard, QWidget *parent)
	: EstateDlg(parent)
	, board(newBoard)
{
	QVBoxLayout *vlayout = new QVBoxLayout(this);
	vlayout->setMargin(0);

	QHBoxLayout *layout = new QHBoxLayout();
	vlayout->addLayout(layout);

	layout->addWidget(new QLabel(i18n("Cards from"), this));

	cards = new CardStackComboBox(board, this);
	layout->addWidget(cards);
}

void CardsDlg::save()
{
	estate->setTakeCard(cards->currentText());
}

void CardsDlg::update()
{
	cards->setCurrentCardStack(estate->takeCard());
}

/////////////////////////////////

StreetDlg::StreetDlg(ConfigBoard *newBoard, QWidget *parent)
	: EstateDlg(parent)
	, board(newBoard)
{
	QVBoxLayout *bigbox = new QVBoxLayout(this);
	bigbox->setMargin(0);

	QGroupBox *RentPage = new QGroupBox(i18n("&Rent by Number of Houses"), this);
	bigbox->addWidget(RentPage);
	QGridLayout *rentBox = new QGridLayout(RentPage);
	rentBox->addWidget(new QLabel(i18n("None:"), RentPage), 0, 0);
	rentBox->addWidget(new QLabel(i18n("One:"), RentPage), 0, 1);
	rentBox->addWidget(new QLabel(i18n("Two:"), RentPage), 0, 2);
	rentBox->addWidget(new QLabel(i18n("Three:"), RentPage), 2, 0);
	rentBox->addWidget(new QLabel(i18n("Four:"), RentPage), 2, 1);
	rentBox->addWidget(new QLabel(i18n("Hotel:"), RentPage), 2, 2);

	rentBox->addWidget(houses0 = new QSpinBox(RentPage), 1, 0);
	rentBox->addWidget(houses1 = new QSpinBox(RentPage), 1, 1);
	rentBox->addWidget(houses2 = new QSpinBox(RentPage), 1, 2);
	rentBox->addWidget(houses3 = new QSpinBox(RentPage), 3, 0);
	rentBox->addWidget(houses4 = new QSpinBox(RentPage), 3, 1);
	rentBox->addWidget(houses5 = new QSpinBox(RentPage), 3, 2);
	houses0->setMinimum(0);
	houses0->setMaximum(3000);
	houses0->setSuffix(i18n("$"));
	houses0->setSpecialValueText(i18n("None"));
	houses1->setMinimum(0);
	houses1->setMaximum(3000);
	houses1->setSuffix(i18n("$"));
	houses1->setSpecialValueText(i18n("None"));
	houses2->setMinimum(0);
	houses2->setMaximum(3000);
	houses2->setSuffix(i18n("$"));
	houses2->setSpecialValueText(i18n("None"));
	houses3->setMinimum(0);
	houses3->setMaximum(3000);
	houses3->setSuffix(i18n("$"));
	houses3->setSpecialValueText(i18n("None"));
	houses4->setMinimum(0);
	houses4->setMaximum(3000);
	houses4->setSuffix(i18n("$"));
	houses4->setSpecialValueText(i18n("None"));
	houses5->setMinimum(0);
	houses5->setMaximum(3000);
	houses5->setSuffix(i18n("$"));
	houses5->setSpecialValueText(i18n("None"));

	QGridLayout *pricesBox = new QGridLayout();
	bigbox->addLayout(pricesBox);
	pricesBox->addWidget(new QLabel(i18n("Price:"), this), 0, 0);
	pricesBox->addWidget(price = new QSpinBox(this), 0, 1);
	price->setMinimum(0);
	price->setMaximum(3000);
	price->setSingleStep(25);
	price->setSpecialValueText(i18n("None"));
	price->setSuffix(i18n("$"));

	QLabel *groupLabel = new QLabel(i18n("Group:"), this);
	pricesBox->addWidget(groupLabel, 1, 0);
	groupCombo = new ConfigEstateGroupComboBox(board, this);
	groupCombo->setObjectName( "Group Combo");
	groupChanged(groupCombo->currentText());
	connect(groupCombo, SIGNAL(activated(const QString &)), this, SLOT(groupChanged(const QString &)));

	pricesBox->addWidget(groupCombo, 1, 1);
}

void StreetDlg::groupChanged(const QString &groupName)
{
	foreach (const ConfigEstateGroup &group, board->estateGroups())
	{
		if (group.name() == groupName)
		{
			if (group.fgColor().isValid())
				estate->setColor(group.fgColor());
			if (group.bgColor().isValid())
				estate->setBgColor(group.bgColor());
			estate->update();

			break;
		}
	}
}

void StreetDlg::save()
{
	estate->setRent(0, houses0->value());
	estate->setRent(1, houses1->value());
	estate->setRent(2, houses2->value());
	estate->setRent(3, houses3->value());
	estate->setRent(4, houses4->value());
	estate->setRent(5, houses5->value());
	estate->setPrice(price->value());
	estate->setGroup(groupCombo->currentText());
}

void StreetDlg::update()
{
	houses0->setValue(estate->rent(0));
	houses1->setValue(estate->rent(1));
	houses2->setValue(estate->rent(2));
	houses3->setValue(estate->rent(3));
	houses4->setValue(estate->rent(4));
	houses5->setValue(estate->rent(5));

	price->setValue(estate->price());

	groupCombo->setCurrentGroup(estate->group());

	groupChanged(estate->group());
}

/////////////////////////////////

JailDlg::JailDlg(QWidget *parent)
	: GenericDlg(parent)
{
	QHBoxLayout *hlayout = new QHBoxLayout();
	layout()->addItem(hlayout);
	hlayout->addWidget(new QLabel(i18n("Pay amount:"), this));

	hlayout->addWidget(payAmount = new QSpinBox(this));
	payAmount->setMinimum(0);
	payAmount->setMaximum(1000);
	payAmount->setSuffix(i18n("$"));
	payAmount->setSpecialValueText(i18n("None"));
	hlayout->addWidget(payAmount);
}

void JailDlg::save()
{
	GenericDlg::save();
	estate->setPayAmount(payAmount->value());
}

void JailDlg::update()
{
	GenericDlg::update();
	payAmount->setValue(estate->payAmount());
}

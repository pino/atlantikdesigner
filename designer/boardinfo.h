#ifndef ATLANTIK_BOARDINFO_H
#define ATLANTIK_BOARDINFO_H

#include <qstringlist.h>

#include <kpagedialog.h>

class KColorButton;

class QVBoxLayout;
class QSpinBox;
class QPushButton;
class QLineEdit;

class ConfigBoard;

class LotsaEdits : public QWidget
{
	Q_OBJECT

	public:
	LotsaEdits(bool, const QStringList &defaults = QStringList(), QWidget *parent = nullptr);

	public:
	QStringList readRows() const;

	private Q_SLOTS:
	void more();
	void less();

	private:
	QWidget *newRow(const QString &text = QString());

	bool editable;
	QList<QWidget *> list;
	QVBoxLayout *layout;
	QPushButton *lessButton;
};

class BoardInfoDlg : public KPageDialog
{
	Q_OBJECT
	
	public:
	BoardInfoDlg(bool editable, ConfigBoard *, QWidget *parent = nullptr);

	protected Q_SLOTS:
	void slotOk();
	void slotApply();

	private:
	QLineEdit *name;
	QLineEdit *url;
	QLineEdit *version;
	QLineEdit *description;
	KColorButton *bgColor;
	QSpinBox *minPlayers;
	QSpinBox *maxPlayers;
	QSpinBox *houses;
	QSpinBox *hotels;
	QSpinBox *startMoney;

	LotsaEdits *authors;
	LotsaEdits *credits;

	ConfigBoard *board;
};

#endif

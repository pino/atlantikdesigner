// Copyright (c) 2015 Pino Toscano <pino@kde.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version
// 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING.  If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.

#ifndef ATLANTIKDESIGNER_MATHEDIT_H
#define ATLANTIKDESIGNER_MATHEDIT_H

#include <QRegularExpression>
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QTextEdit>

class MathHighlighter : public QSyntaxHighlighter
{
	public:
	MathHighlighter(QTextDocument *parent);

	public:
	static const QStringList allowedVars;

	protected:
	void highlightBlock(const QString &text) override;

	private:
	QRegularExpression m_varsRe;
	QTextCharFormat m_formatVarBase;
	QTextCharFormat m_formatVarError;
};

class MathEdit : public QTextEdit
{
	Q_OBJECT

	public:
	MathEdit(QWidget *parent = nullptr);
	QSize sizeHint() const override;
	QString text() const { return toPlainText(); }
	void setText(const QString &text) { setPlainText(text); }

	Q_SIGNALS:
	void textChanged(const QString &);

	protected:
	void keyPressEvent(QKeyEvent *event) override;

	private Q_SLOTS:
	void slotTextChanged();
};

#endif

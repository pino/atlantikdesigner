// Copyright (c) 2015 Pino Toscano <pino@kde.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version
// 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING.  If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.

#include "cards.h"

#include <QCheckBox>
#include <QSpinBox>
#include <QIcon>
#include <QInputDialog>
#include <QComboBox>

#include <kmessagebox.h>

#include "editor.h"
#include "mathedit.h"

CardPropertyWidget::CardPropertyWidget(ConfigBoard *newBoard, int aid, Card *acard, QWidget *parent)
	: QWidget(parent)
	, card(acard)
	, value(nullptr)
	, estate(nullptr)
	, estateGroup(nullptr)
	, math(nullptr)
	, check(nullptr)
	, prevType(CAV_Unknown)
	, board(newBoard)
	, id(aid)
{
	hlayout = new QHBoxLayout(this);
	hlayout->setMargin(0);
	typeCombo = new QComboBox(this);
	typeCombo->addItems(allCardActionDescriptions());
	hlayout->addWidget(typeCombo);
	connect(typeCombo, SIGNAL(activated(int)), this, SLOT(typeChanged(int)));

	hlayout->addStretch();

	typeCombo->setCurrentIndex(findCardAction(card->properties[id].key));
	typeChanged(typeCombo->currentIndex());
}

void CardPropertyWidget::valueChanged(int i)
{
	if (!value)
		return;
	card->properties[id].value = i;
	value->setValue(i);
}

void CardPropertyWidget::estateChanged(int i)
{
	if (!estate)
		return;

	card->properties[id].value = i;
	estate->setCurrentIndex(i);
}

void CardPropertyWidget::estateGroupChanged(const QString &s)
{
	if (!estateGroup)
		return;

	card->properties[id].value = s;
}

void CardPropertyWidget::mathChanged(const QString &s)
{
	if (!math)
		return;

	card->properties[id].value = s;
}

void CardPropertyWidget::typeChanged(int i)
{
	const CardAction action = getCardAction(i);
	CardActionValueType type = action.type;
	// always change the key to the new type selected,
	// but reuse the widget if the action type if the same
	card->properties[id].key = action.key;
	if (type == prevType)
		return;

	delete estate;
	estate = nullptr;
	delete value;
	value = nullptr;
	delete estateGroup;
	estateGroup = nullptr;
	delete math;
	math = nullptr;
	delete check;
	check = nullptr;

	const QVariant v = card->properties[id].value;

	switch (type)
	{
	case CAV_Unknown:
		Q_ASSERT_X(false, "CardPropertyWidget::typeChanged", "CAV_Unknown");
		break;

	case CAV_Integer:
	case CAV_Money:
	case CAV_EstateCount:
	{
		QString suffix;

		value = new QSpinBox(this);
		value->setMinimum(0);
		value->setMaximum(2000);
		value->setSingleStep(type == CAV_EstateCount ? 1 : 5);
		if (type == CAV_Money)
			suffix = "$";
		else if (type == CAV_EstateCount)
			suffix = i18n("Estate(s)").prepend(" ");
		value->setSuffix(suffix);
		connect(value, SIGNAL(valueChanged(int)), this, SLOT(valueChanged(int)));

		hlayout->addWidget(value);
		valueChanged(v.toInt());
		break;
	}

	case CAV_Bool:
		check = new QCheckBox(this);
		check->setEnabled(false);
		check->setChecked(true);

		hlayout->addWidget(check);
		break;

	case CAV_Estate:
	{
		estate = new QComboBox(this);
		QStringList estateStrings;
		foreach (ConfigEstate *curestate, board->estates())
			estateStrings.append(curestate->name());
		estate->addItems(estateStrings);
		connect(estate, SIGNAL(activated(int)), this, SLOT(estateChanged(int)));

		hlayout->addWidget(estate);
		estateChanged(v.toInt());
		break;
	}

	case CAV_EstateGroup:
	{
		estateGroup = new ConfigEstateGroupComboBox(board, this);
		connect(estateGroup, SIGNAL(activated(QString)), this, SLOT(estateGroupChanged(QString)));

		hlayout->addWidget(estateGroup);
		estateGroup->setCurrentGroup(v.toString());
		break;
	}

	case CAV_Math:
		math = new MathEdit(this);
		connect(math, SIGNAL(textChanged(QString)), this, SLOT(mathChanged(QString)));

		hlayout->addWidget(math);
		math->setText(v.toString());
	}

	prevType = type;
}

CardsEditor::CardsEditor(ConfigBoard *newBoard, QWidget *parent)
	: QDialog(parent)
	, mylist(newBoard->cards())
	, board(newBoard)
{
	setWindowTitle(i18n("Cards Editor"));
	setAttribute(Qt::WA_DeleteOnClose, true);

	const QIcon addIcon = QIcon::fromTheme("list-add");
	const QIcon removeIcon = QIcon::fromTheme("list-remove");

	m_ui.setupUi(this);

	m_ui.addStack->setIcon(addIcon);
	m_ui.addCard->setIcon(addIcon);
	m_ui.addProperty->setIcon(addIcon);
	m_ui.deleteCard->setIcon(removeIcon);
	m_ui.deleteProperty->setIcon(removeIcon);
	m_ui.renameCard->setIcon(QIcon::fromTheme("edit-rename"));
	m_ui.cardsGroupBox->setEnabled(false);
	m_ui.cardsGroupBox->layout()->setMargin(0);
	m_ui.propertyButtonsLayout->setAlignment(Qt::AlignTop | Qt::AlignRight);

	connect(m_ui.buttonBox,SIGNAL(rejected()),this,SLOT(reject()));
	connect(m_ui.buttonBox,SIGNAL(accepted()),this,SLOT(slotOk()));
	connect(m_ui.buttonBox->button(QDialogButtonBox::Apply),SIGNAL(clicked()),this,SLOT(slotApply()));
	connect(m_ui.stacksCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotChangeStack(int)));
	connect(m_ui.addStack, SIGNAL(clicked()), this, SLOT(slotAddStack()));
	connect(m_ui.cardsList, SIGNAL(currentRowChanged(int)), this, SLOT(slotChangeCard(int)));
	connect(m_ui.cardsList, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(slotCardChanged(QListWidgetItem*)));
	connect(m_ui.addCard, SIGNAL(clicked()), this, SLOT(slotAddCard()));
	connect(m_ui.renameCard, SIGNAL(clicked()), this, SLOT(slotRenameCard()));
	connect(m_ui.deleteCard, SIGNAL(clicked()), this, SLOT(slotDeleteCard()));
	connect(m_ui.addProperty, SIGNAL(clicked()), this, SLOT(slotAddProperty()));
	connect(m_ui.deleteProperty, SIGNAL(clicked()), this, SLOT(slotDeleteProperty()));

	QStringList stacks;
	foreach (const CardStack &stack, mylist)
		stacks.append(stack.name());
	m_ui.stacksCombo->addItems(stacks);
}

void CardsEditor::slotApply()
{
	board->setCards(mylist);
}

void CardsEditor::slotOk()
{
	slotApply();

	accept();
}

void CardsEditor::slotChangeStack(int index)
{
	clearPropertyWidgets();
	m_ui.cardsList->clear();
	m_currentStack = nullptr;
	m_ui.cardsGroupBox->setEnabled(false);
	m_ui.addCard->setEnabled(false);
	m_ui.addProperty->setEnabled(false);

	if (index == -1)
		return;

	m_currentStack = &mylist[index];

	QStringList cards;
	foreach (const Card &card, *m_currentStack)
		cards.append(card.name);
	m_ui.cardsList->addItems(cards);
	m_ui.cardsGroupBox->setEnabled(true);
	m_ui.addCard->setEnabled(true);
}

void CardsEditor::slotAddStack()
{
	bool ok;
	const QString name = QInputDialog::getText(this, i18n("Add Stack"), i18n("Enter the name of the new stack:"), QLineEdit::Normal, QString(), &ok);
	if (!ok)
		return;

	foreach (const CardStack &stack, mylist)
	{
		if (stack.name() == name)
		{
			KMessageBox::information(this, i18n("That name is already on the list."));
			return;
		}
	}

	mylist.append(CardStack(name));
	m_ui.stacksCombo->addItem(name);
	m_ui.stacksCombo->setCurrentIndex(m_ui.stacksCombo->count() - 1);
}

void CardsEditor::slotChangeCard(int index)
{
	clearPropertyWidgets();

	if (index == -1 || index >= m_currentStack->count())
		return;

	m_currentCard = &((*m_currentStack)[index]);

	for (int i = 0; i < m_currentCard->properties.count(); ++i)
	{
		CardPropertyWidget *w = new CardPropertyWidget(board, m_propertyWidgets.count(), m_currentCard, this);
		m_propertyWidgets.append(w);
		m_ui.propertiesLayout->insertWidget(m_ui.propertiesLayout->count() - 1, w);
	}

	m_ui.renameCard->setEnabled(true);
	m_ui.deleteCard->setEnabled(true);
	m_ui.addProperty->setEnabled(true);
	m_ui.deleteProperty->setEnabled(!m_propertyWidgets.isEmpty());
}

void CardsEditor::slotCardChanged(QListWidgetItem *item)
{
	if (!m_currentCard)
		return;

	m_currentCard->name = item->text();
}

void CardsEditor::slotAddCard()
{
	bool ok = false;
	const QString name = QInputDialog::getText(this, i18n("Add Card"), i18n("Enter the text of the new card:"), QLineEdit::Normal, QString(), &ok);
	if (!ok)
		return;

	m_ui.cardsList->addItem(name);
	m_currentStack->append(Card(name));
	m_ui.cardsList->setCurrentRow(m_ui.cardsList->count() - 1);
}

void CardsEditor::slotRenameCard()
{
	if (!m_currentCard)
		return;

	bool ok = false;
	const QString name = QInputDialog::getText(this, i18n("Rename Card"), i18n("Change the text of the card:"), QLineEdit::Normal, m_currentCard->name, &ok);
	if (!ok)
		return;

	// this will trigger itemChanged
	m_ui.cardsList->currentItem()->setText(name);
}

void CardsEditor::slotDeleteCard()
{
	if (!m_currentCard)
		return;

	const int index = m_ui.cardsList->currentRow();
	m_currentStack->removeAt(index);
	// this will trigger currentRowChanged
	delete m_ui.cardsList->currentItem();
}

void CardsEditor::slotAddProperty()
{
	if (!m_currentCard)
		return;

	m_currentCard->properties.append(CardProperty("pay", 0));
	CardPropertyWidget *w = new CardPropertyWidget(board, m_propertyWidgets.count(), m_currentCard, this);
	m_propertyWidgets.append(w);
	m_ui.propertiesLayout->insertWidget(m_ui.propertiesLayout->count() - 1, w);
	m_ui.deleteProperty->setEnabled(true);
}

void CardsEditor::slotDeleteProperty()
{
	if (!m_currentCard)
		return;

	delete m_propertyWidgets.takeLast();
	m_currentCard->properties.pop_back();
	m_ui.deleteProperty->setEnabled(!m_propertyWidgets.isEmpty());
}

void CardsEditor::clearPropertyWidgets()
{
	qDeleteAll(m_propertyWidgets);
	m_propertyWidgets.clear();
	m_currentCard = nullptr;
	m_ui.renameCard->setEnabled(false);
	m_ui.deleteCard->setEnabled(false);
	m_ui.deleteProperty->setEnabled(false);
}


#include <qpalette.h>
#include <qevent.h>
#include <qfile.h>
#include <qpoint.h>
#include <qtimer.h>
#include <QVBoxLayout>
#include <QCloseEvent>
#include <QIcon>
#include <QFileDialog>
#include <QUrl>
#include <QKeySequence>

#include <kactioncollection.h>
#include <kconfig.h>
#include <klocalizedstring.h>
#include <kmessagebox.h>
#include <kstandardaction.h>
#include <kselectaction.h>
#include <krecentfilesaction.h>
#include <kconfiggroup.h>
#include <ksharedconfig.h>

#include <estate.h>
#include <player.h>

#include "boardinfo.h"
#include "editor.h"
#include "designer.h"
#include "board.h"
#include "cards.h"
#include "group.h"

AtlanticDesigner::AtlanticDesigner(QWidget *parent)
	: KXmlGuiWindow(parent)
	, editor(nullptr)
	, board(nullptr)
	, boardData(nullptr)
	, copiedEstate(nullptr)
	, m_player(nullptr)
	, isMod(false)
	, m_tokenTheme(TokenTheme::defaultTheme())
{
	QAction *a = nullptr;

	(void) KStandardAction::quit(this, SLOT(close()), actionCollection());
	(void) KStandardAction::open(this, SLOT(open()), actionCollection());
	(void) KStandardAction::openNew(this, SLOT(openNew()), actionCollection());
	(void) KStandardAction::save(this, SLOT(save()), actionCollection());
	(void) KStandardAction::saveAs(this, SLOT(saveAs()), actionCollection());

        a = actionCollection()->addAction( "boardinfo" );
	a->setText(i18n("&Edit Gameboard Info..."));
	actionCollection()->setDefaultShortcut(a, QKeySequence(Qt::CTRL+Qt::Key_I));
	connect(a, SIGNAL(triggered()), this, SLOT(info()));

        a = actionCollection()->addAction( "groupeditor" );
	a->setText(i18n("&Edit Groups..."));
	actionCollection()->setDefaultShortcut(a, QKeySequence(Qt::CTRL+Qt::Key_G));
	connect(a, SIGNAL(triggered()), this, SLOT(editGroups()));

        a = actionCollection()->addAction("cardseditor");
	a->setText(i18n("&Edit Cards..."));
	connect(a, SIGNAL(triggered()), this, SLOT(editCards()));

        a = actionCollection()->addAction( "larger" );
	a->setText(i18n("&Add 4 Squares"));
	a->setIcon(QIcon::fromTheme("zoom-in"));
	connect(a, SIGNAL(triggered()), this, SLOT(larger()));

        a = actionCollection()->addAction( "smaller" );
	a->setText(i18n("&Remove 4 Squares"));
	a->setIcon(QIcon::fromTheme("zoom-out"));
	connect(a, SIGNAL(triggered()), this, SLOT(smaller()));

	(void) KStandardAction::copy(this, SLOT(copy()), actionCollection());
	(void) KStandardAction::paste(this, SLOT(paste()), actionCollection());

        a = actionCollection()->addAction( "up" );
	a->setText(i18n("&Up"));
	actionCollection()->setDefaultShortcut(a, QKeySequence(Qt::Key_Up));
	connect(a, SIGNAL(triggered()), this, SLOT(up()));

        a = actionCollection()->addAction( "down" );
	a->setText(i18n("&Down"));
	actionCollection()->setDefaultShortcut(a, QKeySequence(Qt::Key_Down));
	connect(a, SIGNAL(triggered()), this, SLOT(down()));

        a = actionCollection()->addAction( "left" );
	a->setText(i18n("&Left"));
	actionCollection()->setDefaultShortcut(a, QKeySequence(Qt::Key_Left));
	connect(a, SIGNAL(triggered()), this, SLOT(left()));

        a = actionCollection()->addAction( "right" );
	a->setText(i18n("&Right"));
	actionCollection()->setDefaultShortcut(a, QKeySequence(Qt::Key_Right));
	connect(a, SIGNAL(triggered()), this, SLOT(right()));

	recentAct = KStandardAction::openRecent(this, SLOT(openRecent(QUrl)), actionCollection());
	recentAct->loadEntries(KSharedConfig::openConfig()->group( "Designer recent files") );

        estateAct = actionCollection()->add<KSelectAction>( "estate_num" );
        estateAct->setText( i18n("Change Estate") );
	connect(estateAct, SIGNAL(triggered(int)), SLOT(changeEstate(int)));

	setupGUI(QSize(350, 400), StandardWindowOption(Default & ~StatusBar));

	KConfigGroup config(KSharedConfig::openConfig(), "General");
	QColor defaultColor = palette().window().color();
	defaultBg = config.readEntry("alternateBackground", defaultColor);
	config = KConfigGroup(KSharedConfig::openConfig(), "WM");
	defaultColor = palette().dark().color();
	defaultFg = config.readEntry("activeBackground", defaultColor);

	openNew();
}

AtlanticDesigner::~AtlanticDesigner()
{
	delete boardData;
	delete m_player;
}

void AtlanticDesigner::setConfigBoard(ConfigBoard *newBoard)
{
	// while it would be nice to update the dialogs with the data
	// of the new board, it is a bit too much hassle for now
	delete groupEditor;
	delete boardInfoDlg;
	delete cardsEditor;

	copiedEstate = nullptr;

	delete boardData;

	boardData = newBoard;
	connect(boardData, SIGNAL(cardsChanged()), this, SLOT(modified()));
	connect(boardData, SIGNAL(infoChanged()), this, SLOT(updateBackground()));
	connect(boardData, SIGNAL(infoChanged()), this, SLOT(modified()));
	connect(boardData, SIGNAL(estateGroupsChanged()), this, SLOT(groupsChanged()));
	connect(boardData, SIGNAL(estateGroupsChanged()), this, SLOT(modified()));
}

void AtlanticDesigner::initBoard()
{
	// let her say her prayers (if she's alive)
	if (editor)
		editor->aboutToDie();

	delete editor;
	delete board;

	board = new AtlantikBoard(nullptr, max, AtlantikBoard::Edit, this);
	board->setObjectName("Board");
	board->setTokenTheme(m_tokenTheme);

	setCentralWidget(board);
	QVBoxLayout *layout = new QVBoxLayout(board->centerWidget());
	editor = new EstateEdit(boardData, board->centerWidget());
        editor->setObjectName("Estate Editor");
	connect(editor, SIGNAL(goChecked(int)), this, SLOT(goChanged(int)));
	connect(editor, SIGNAL(updateBackground()), this, SLOT(updateBackground()));
	connect(editor, SIGNAL(resized()), board, SLOT(slotResizeAftermath()));
	layout->addWidget(editor);

	editor->setReady(false);

	board->show();
	editor->show();

	initToken();
}

void AtlanticDesigner::initToken()
{
	if (!m_player)
		m_player = new Player(0);
	else
		m_player->setLocation(nullptr);

	board->addToken(m_player);

	connect(m_player, SIGNAL(changed(Player *)), board, SLOT(playerChanged(Player *)));
}

void AtlanticDesigner::openNew()
{
	if (warnClose())
		return;
	m_filename = QString();

	max = 40;

	ConfigBoard *newBoard = new ConfigBoard();
	ConfigEstateGroup defaultGroup("Default");
	defaultGroup.setBgColor(defaultBg);
	defaultGroup.setFgColor(defaultFg);
	newBoard->setEstateGroups(ConfigEstateGroupList() << defaultGroup);
	setConfigBoard(newBoard);
	initBoard();

	for (int i = 0; i < 40; ++i)
	{
		(void) newEstate(i);
	}

	doCaption(false);
	updateJumpMenu();

	groupsChanged();
	updateBackground();

	QTimer::singleShot(500, this, SLOT(setPlayerAtBeginning()));
	doCaption(false);
}

ConfigEstate *AtlanticDesigner::newEstate(int i)
{
	ConfigEstate *estate = boardData->newEstate(i);
	estate->setName(i18n("New Estate"));
	estate->setPrice(100);
	for (int j = 0; j < 6; ++j)
		estate->setRent(j, 10 * (j + 1));
	estate->setChanged(false);

	connect(estate, SIGNAL(LMBClicked(Estate *)), this, SLOT(changeEstate(Estate *)));
	connect(estate, SIGNAL(changed()), this, SLOT(modified()));

	board->addEstateView(estate);

	return estate;
}

bool AtlanticDesigner::warnClose()
{
	if (isMod)
	{
		int result = KMessageBox::warningYesNoCancel(this, i18n("There are unsaved changes to gameboard. Save them?"), i18n("Unsaved Changes"), KStandardGuiItem::save(), KStandardGuiItem::discard(), KStandardGuiItem::cancel(), "DiscardAsk");
		switch(result)
		{
		case KMessageBox::Yes:
			save();
			// fallthrough
		case KMessageBox::No:
			return false;
		case KMessageBox::Cancel:
			return true;
		}
	}
	return false;
}

void AtlanticDesigner::open()
{
	if (warnClose())
		return;

	const QString filename = QFileDialog::getOpenFileName(this);

	if (filename.isEmpty())
		return;

	if (!openFile(filename))
		return;

	recentAct->addUrl(QUrl::fromLocalFile(filename));
}

void AtlanticDesigner::openRecent(const QUrl &url)
{
	if (warnClose())
		return;

	if (url.isLocalFile())
	{
		openFile(url.toLocalFile());
	}
	else
	{
		recentAct->removeUrl(url);
	}
}

bool AtlanticDesigner::openFile(const QString &filename)
{
	QFile f(filename);
	if (!f.open(QIODevice::ReadOnly))
		return false;

	ConfigBoard *newBoard = ConfigBoard::read(&f);
	if (!newBoard)
	{
		KMessageBox::sorry(this, i18n("Error while reading %1.", filename));
		return false;
	}

	const int count = newBoard->estates().count();
	if (count < 8)
	{
		KMessageBox::detailedSorry(this, i18n("This board file is invalid; cannot open."), i18n("There are only %1 estates specified in this file.", count));
		return false;
	}

	setConfigBoard(newBoard);
	max = count;
	m_filename = filename;

	initBoard();
	foreach (ConfigEstate *estate, boardData->estates())
	{
		connect(estate, SIGNAL(LMBClicked(Estate *)), this, SLOT(changeEstate(Estate *)));
		connect(estate, SIGNAL(changed()), this, SLOT(modified()));

		board->addEstateView(estate);
	}

	groupsChanged();
	updateBackground();
	updateJumpMenu();

	QTimer::singleShot(500, this, SLOT(setPlayerAtBeginning()));
	doCaption(false);

	return true;
}

void AtlanticDesigner::updateJumpMenu()
{
	QStringList estates;
	for (int i = 1; i <= max; i++)
		estates.append(i18n("Jump to Estate %1", i));
	estateAct->setItems(estates);
}

void AtlanticDesigner::setPlayerAtBeginning()
{
	ConfigEstate *go = boardData->findGo();

	if (!go)
		go = boardData->estateAt(0);

	movePlayer(go);
	editor->setEstate(go);
}

void AtlanticDesigner::saveAs()
{
	const QString filename = QFileDialog::getSaveFileName(this);
	if (filename.isEmpty())
		return;

	m_filename = filename;
	save();
	recentAct->addUrl(QUrl::fromLocalFile(m_filename));
}

void AtlanticDesigner::save()
{
	// if there is no current file name, act as a "save as";
	// it will call us back again, but with a non-empty filename
	if (m_filename.isEmpty())
		return saveAs();

	(void) editor->saveEstate();

	QFile f(m_filename);
	if (!f.open(QIODevice::WriteOnly))
		return;

	boardData->save(&f);

	f.flush();
	doCaption(false);
}

void AtlanticDesigner::copy()
{
	copiedEstate = editor->theEstate();
}

void AtlanticDesigner::paste()
{
	if (!copiedEstate)
		return;

	ConfigEstate *estate = editor->theEstate();
	if (copiedEstate->id() == estate->id())
		return;

	for (int i = 0; i < 6; i++)
		estate->setRent(i, copiedEstate->rent(i));
	estate->setPrice(copiedEstate->price());
	estate->setTax(copiedEstate->tax());
	estate->setTaxPercentage(copiedEstate->taxPercentage());
	estate->setGroup(copiedEstate->group());
	estate->setTakeCard(copiedEstate->takeCard());
	estate->setType(copiedEstate->type());
	estate->setName(copiedEstate->name());
	estate->setPassMoney(copiedEstate->passMoney());

	editor->setEstate(estate);
	estate->update();

	groupsChanged();
}

void AtlanticDesigner::closeEvent(QCloseEvent *e)
{
	if (warnClose())
	{
		e->ignore();
		return;
	}

	recentAct->saveEntries(KSharedConfig::openConfig()->group( "Designer recent files") );

	e->accept();
}

void AtlanticDesigner::changeEstate(int index)
{
	changeEstate(boardData->estateAt(index));
}

void AtlanticDesigner::changeEstate(Estate *estate)
{
	if (!estate)
		return;

	setFocus();

	(void) editor->saveEstate();

	editor->setEstate(static_cast<ConfigEstate *>(estate));
	movePlayer(estate);
}

void AtlanticDesigner::movePlayer(Estate *estate)
{
	estateAct->setCurrentItem(estate->id());
	board->setFocus();
	m_player->setLocation(estate);
	m_player->update();
}

// this will add a square to all sides
void AtlanticDesigner::larger()
{
	max += 4;
	int sideLen = max/4;

	initBoard();

	for (int i = 0; i < max; ++i)
	{
		ConfigEstate *estate = boardData->estateAt(i);
		estate->setEstateId(i);
		board->addEstateView(estate);

			// make a newEstate
		if ((i % sideLen - 1) == 0 || i == 1)
		{
			newEstate(i);
		}
	}

	updateJumpMenu();
	groupsChanged();
	updateBackground();

	QTimer::singleShot(500, this, SLOT(setPlayerAtBeginning()));
	doCaption(true);
}

void AtlanticDesigner::smaller()
{
	if (max < 12)
		return;

	max -= 4;
	int sideLen = max/4;

	initBoard();

	bool remove = true;

	ConfigEstate *estate = nullptr;
	for (int i = 0; i < max; ++i)
	{
		// remove estate
		if (((i % sideLen - 1) == 0 || i == 1) && remove)
		{
			boardData->delEstate(i);
			i--;
			remove = false;
		}
		else
		{
			estate = boardData->estateAt(i);
			estate->setEstateId(i);
			board->addEstateView(estate);

			remove = true;
		}
	}

	updateJumpMenu();
	groupsChanged();
	updateBackground();
	QTimer::singleShot(500, this, SLOT(setPlayerAtBeginning()));

	doCaption(true);
}

void AtlanticDesigner::modified()
{
	doCaption(true);
}

void AtlanticDesigner::doCaption(bool modified)
{
	setCaption(m_filename, modified);
	isMod = modified;
}

void AtlanticDesigner::editGroups()
{
	if (groupEditor.isNull())
	{
		groupEditor = new GroupEditor(boardData, this);
	}

	// non-modal
	groupEditor->show();
}

void AtlanticDesigner::goChanged(int id)
{
	foreach (ConfigEstate *curEstate, boardData->estates())
		if (curEstate->go() && curEstate->id() != id)
			curEstate->setGo(false);
}

void AtlanticDesigner::groupsChanged()
{
	const BoardInfo boardInfo = boardData->info();

	foreach (ConfigEstate *curEstate, boardData->estates())
	{
		foreach (const ConfigEstateGroup &group, boardData->estateGroups())
		{
			if (group.name() == curEstate->group() && curEstate->type() == Street)
			{
				curEstate->setBgColor(group.bgColor().isValid()? group.bgColor() : boardInfo.bgColor);
				curEstate->setColor(group.fgColor());
				curEstate->update();
				break;
			}
		}
	}
}

void AtlanticDesigner::updateBackground()
{
	const BoardInfo boardInfo = boardData->info();

	foreach (ConfigEstate *curEstate, boardData->estates())
	{
		if (curEstate->type() != Street || (curEstate->type() == Street && curEstate->group().isNull()))
		{
			curEstate->setColor(QColor());
			curEstate->setBgColor(boardInfo.bgColor);
		}

		if (curEstate->forceBg().isValid() && (curEstate->type() == FreeParking || curEstate->type() == Jail || curEstate->type() == ToJail))
		{
			curEstate->setBgColor(curEstate->forceBg());
		}

		curEstate->update();
	}
}

void AtlanticDesigner::info()
{
	if (boardInfoDlg.isNull())
	{
		boardInfoDlg = new BoardInfoDlg(true, boardData, this);
	}

	// non-modal
	boardInfoDlg->show();
}

void AtlanticDesigner::editCards()
{
	if (cardsEditor.isNull())
	{
		cardsEditor = new CardsEditor(boardData, this);
	}

	// non-modal
	cardsEditor->show();
}

// now some fun functions ;)

void AtlanticDesigner::up()
{
	if (editor->upArrow())
		return;
	int fourth = max / 4;
	int estateId = editor->theEstate()->id() + 1;
	int dest = estateId - 1;

	if (estateId <= 2*fourth && estateId > fourth) // left side
		dest++;
	else if (estateId > (3*fourth + 1)) // right side
		dest--;
	else if (estateId == 1)
		dest = max - 1;

	changeEstate(dest);
}

void AtlanticDesigner::down()
{
	if (editor->downArrow())
		return;
	int fourth = max / 4;
	int estateId = editor->theEstate()->id() + 1;
	int dest = estateId - 1;

	if (estateId <= (2*fourth + 1) && estateId > (fourth + 1)) // left side
		dest--;
	else if (estateId > 3*fourth && estateId < max) // right side
		dest++;
	else if (estateId == max)
		dest = 0;

	changeEstate(dest);
}

void AtlanticDesigner::left()
{
	if (editor->leftArrow())
		return;
	int fourth = max / 4;
	int estateId = editor->theEstate()->id() + 1;
	int dest = estateId - 1;

	if (estateId <= fourth) // bottom
		dest++;
	else if (estateId > (2*fourth + 1) && estateId <= (3*fourth + 1)) // top
		dest--;

	changeEstate(dest);
}

void AtlanticDesigner::right()
{
	if (editor->rightArrow())
		return;
	int fourth = max / 4;
	int estateId = editor->theEstate()->id() + 1;
	int dest = estateId - 1;

	if (estateId <= (fourth + 1) && estateId != 1) // bottom
		dest--;
	else if (estateId > 2*fourth && estateId <= 3*fourth) // top
		dest++;

	changeEstate(dest);
}
